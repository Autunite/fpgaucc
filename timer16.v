`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    16:21:28 08/18/2017 
// Design Name: 
// Module Name:    timer16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//	This counter counts the period of the fan tachometer
//	and represents it with a 16 bit integer
// it also contains an output for controlling the rest of the fanspeed averaging system
// if the counter rolls over without detecting another tachometer pulse a (2^16)-1 value
// is outputted
// to make the AveEn work as a clocking signal it is set low at count == 1
// upon detecting a rising edge from the fan tachometer the count is written to the 
// output register and the count is set to 0
// uses a synchronous edge detector, thus it will begin and reset the counter
//		two clock cycles after a rising edge is detected
//	there are known bugs at the edge case of the counter being withing
//		a couple of counts from the 16 bit overflow
//		this causes the counter to misreport the fan period by .004578%
//		this edge case is well away from the minimum fan speed so shouldn't
//		matter too much, though it annoys the designer to no end.
//////////////////////////////////////////////////////////////////////////////////
module timer16
(
    input fanInput,
    input clk,
	input reset,
    output reg [15:0] timeOut=16'b1111111111111111,
    output reg  AveEn =0
);
	
	//this reg holds the internal count of the counter
	reg [15:0] count = 0;
	
	//this reg is for rising edge detection and holds the last clocked in state of
	//fanInput
	reg lastInput=0;
	
	//this is another reg for detecting edges, it is put in for countering metastability
	reg secondLastInput=0;
	
	
	//this register keeps the counter from outputting a really high frequency value
	//the first time a rising edge is detected from the fan
	reg firstTime=1;
	
	//this reg works with AveEn and is set internally so that there are no
	//worries about bus contention
	reg internalEnable=0;
	
	always @(posedge clk )
	begin
		//if reset is pulled high
		if(reset == 1)
		begin
			lastInput<=0;
			secondLastInput<=0;
			count <=0;
			//AveEn<=0;
		end
		
		else
		begin
			if(AveEn==1 || count==16'b1111111111111101 || count==16'b1111111111111110 || count==16'b1111111111111111)
			begin
				
				//if AveEn is high then output count
				//reset count to zero
				//may want to try one of the internal edge detect regs for 
				//timing purposes
				if(AveEn==1 && secondLastInput==1)
				begin
					//if its the first rising edge detected count is simply
					//reset to zero. To prevent a super high frequency signal
					//from being outputted
					if(firstTime==1)
					begin
						firstTime<=0;
						count<=0;
					end
					
					//normal count reset behavior on non first time
					else
					begin
						timeOut<=count;
						count<=0;
					end
				end
				
				//if count is at max value
				//Set aveEn to high, 
				//output count
				//set count to 0 (redundant)
				//set first time to equal one,
				//so that if the tach impulse comes in right
				//after, a high frequency spike isn't reported
				if(count==16'b1111111111111101)
				begin
					count<=count+1;
					internalEnable<=1;
				end
				
				if(count==16'b1111111111111110)
				begin
					//timeOut<=count;
					count<=count+1;
					internalEnable<=0;
					firstTime<=1;
					//firstTime<=1;
				end
				
				if(count==16'b1111111111111111)
				begin
					timeOut<=count;
					count<=0;
					
					internalEnable<=0;
				end
				
			end
			//at normal clock rising edge
			else
			begin
				count <=count+1;
			end
			
			//makes sure AveEn is set to zero at
			//count == 16'b0000000000000000
			if(count == 16'b0000000000000000)
			begin
				internalEnable<=0;
			end
			
			lastInput<=fanInput;
			secondLastInput<=lastInput;
			AveEn <= internalEnable | (lastInput & !secondLastInput);
			
		end
	end
	
	
	
endmodule
