`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:58:54 06/20/2018 
// Design Name: 
// Module Name:    clk8_div 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Divide by 8 clock divider used for generating the TX
//		clock
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clk8_div(
    input clk_in,
	 input reset,
    output reg clk_out 
    );
	 
	 reg [1:0] div ;
	 
	 //initial setting of the output to 0 so the simulator
	 //works. Please work simulator
	 //need to get rid of this, while ensuring reset 
	 //zeroes everything
	 /*
	 initial 
	 begin
		clk_out 		<=0;
		div				<=0;
	 end
	 */
	 
	 always@(posedge clk_in)
	 begin
	 
		if(reset == 1)
		begin
			clk_out 		<=0;
			div				<=0;
		end
	 
		else
			div 		<= div + 1;
			if(div == 3)
			begin
				clk_out 	<= ~clk_out;
				div			<=0;
			end
	 end
	 
endmodule
