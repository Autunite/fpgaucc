`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    09:19:02 06/20/2018 
// Design Name: 
// Module Name:    RX_FSM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This is the finite state machine responsible for controlling the receive 
//		data section of the UART and the accompanying modules. It is also responsible for
//		distributing a reset to the UART, command module, and the variable clock generator.
//		Lastly it handles passing data from the UART to the command module.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RX_FSM(
	//input clock set to the same frequency of the RX clock (152 MHz)
    input clk,
	 
	//this is the global reset input, it reads in a global reset and distributes it
	//to the modules under it's control (multispeed clock, command module)
    input global_reset,
	 
	 //this input tells the FSM whether the RX buffer is empty or not, if isn't empty
	 //then it sends commands for the command module to take in the data 
    input rx_empty,
	 
	 //output that tells the RX buffer of the UART to unload data to the output
    output reg uld_rx_data,
	 
	 //output to the command module telling it to load in data
    output reg load,
	 
	 //output to the modules it controls to reset the line as needed
    output reg rx_reset
    );
	 
	 //
	 always@(posedge clk)
	 begin
		casez({global_reset,rx_empty})
		
		//reset is asserted
		2'b1? : 
		begin
			rx_reset <= 1;
		end
		
		//if the UART is emmpty
		//keep outputs at zero
		//rx_empty != 0
		2'b01	:
		begin
			uld_rx_data		<= 0;
			load			<= 0;
			rx_reset		<= 0;
		end
		
		//if UART isn't Empty set both uldrxdata
		//and load to one. 										Test for synchronization problems
		2'b00	:
		begin
			uld_rx_data		<= 1;
			load			<= 1;
			rx_reset		<= 0;
		end
		
		//default reset output set to 0
		//everything else set to 0
		default:
		begin
			uld_rx_data		<= 0;
			load			<= 0;
			rx_reset		<= 0;
		end
			
		endcase
		
	 end


endmodule
