`timescale 1ns / 1ps

//test bench
module SPI_PAR_tb;
				  
	//inputs (make them reg)
	reg DI0;
	reg DI1;
	reg DI2;
	reg DI3;
	reg ADC_CLK;
	reg F_CLK;
	reg RESET;
	
	//input data regs
	reg [15:0] data1;
	reg [15:0] data2;
	reg [15:0] data3;
	reg [15:0] data4;
	
	//outputs (make them wire)
	wire [15:0] ADC_DATA0;
	wire [15:0] ADC_DATA1;
	wire [15:0] ADC_DATA2;
	wire [15:0] ADC_DATA3;
	wire SCLK;
	wire DO;
	
	//instantiate the UUT
	 SPI_PAR uut(
    //inputs
	.DI0(DI0),
	.DI1(DI1),
	.DI2(DI2),
	.DI3(DI3),
	.ADC_CLK(ADC_CLK), //190KHz
	.F_CLK(F_CLK), //400MHz
	.RESET(RESET),
	
	//outputs
	.ADC_DATA0(ADC_DATA0),
	.ADC_DATA1(ADC_DATA1),
	.ADC_DATA2(ADC_DATA2),
	.ADC_DATA3(ADC_DATA3),
	.SCLK(SCLK),
	//.CS(CS),
	.DO(DO)
    ); 
	
	initial begin
	//initialize the inputs
	DI0=0;
	DI1=0;
	DI2=0;
	DI3=0;
	ADC_CLK=1;
	F_CLK=0;
	RESET=0;
	
	//Initialize some registers to feed in to the SPI_PAR
	data1 = 16'hABCD;
	data2 = 16'hBCDE;
	data3 = 16'hCDEF;
	data4 = 16'hDEFA;
	
	
	
	//wait 100 ns for global reset
	#100;
	RESET = 1;
	#40;
	RESET = 0;
	#40;
	
	DI0=data1[15];
	DI1=data2[15];
	DI2=data3[15];
	DI3=data4[15];
	
	//put in stimulus
	//ADC_CLK = 0;
	//#2631;
	//ADC_CLK = 1;
	//#2631;
	 
	end
	
	
	always@(posedge SCLK)
	begin	
		data1 = data1 <<1;
		data2 = data2 <<1;
		data3 = data3 <<1;
		data4 = data4 <<1;
		
		DI0=data1[15];
		DI1=data2[15];
		DI2=data3[15];
		DI3=data4[15];
		
	end
	
	
	//clkA
	always #1.25  F_CLK =  ! F_CLK;
	
	//clkB
	always #2631  ADC_CLK =  ! ADC_CLK;
	
endmodule