`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:09:44 06/21/2018 
// Design Name: 
// Module Name:    UCC_TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: The top module that contains all of the sub modules for the
//		UART controlled clock. This is basically just a wrapper module to make
//		the code easier to maintain.
//
// Dependencies: On most FPGA's this will require a PLL to generate the clocks
//		for the UART RX clock and the fast clock used to generate the sub clocks
//		The higher the frequency on the fast clock the better the resolution you
//		get on the programmable clocks
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: Note the Fast clock is currently set to 400MHz, but can be
//		changed to another frequency rather simply by editing the in_clk reg to the
//		desired frequency in Hz. For example 304MHz might be easier to use and be 
//		less resource intensive. I am currently keeping it at 400MHz because I like
//		having as high of a resolution as possible.
//
// Revision 0.02 - Implementation:
//	In order to implemenent and actually communicate with the outside world the baud
//		rate on the UART will have to be toned down. 800kHz is the theoretical floor
//		on the baud rate before sample rate begins to be degraded. Currently I am 
//		setting the baud rate to be 4.8MHz (76.8 for the rx clk). Which should be readable
//		by my logic analyzer. 
//	Also I am adding in a 1 Hz heartbeat indicator to the FPGA so I know whether my//		programming was successful. Lastly I am going to change how the system behaves
//		on startup so it begins with 7 kilohertz sampling upon reset.
//
//	Revision 0.03 - Integration
//	 This module has been integrated into the greater system. Information from the
//		ADC's will be passed out by both parallel lines and by the TX line. Some of
//		this information goes to the MaxMin module which will help do some of the
//		noise analysis. Hearbeat is also brought out but can be eliminated. But
//		it may be useful for debugging the initial programming of the FPGA
//
//////////////////////////////////////////////////////////////////////////////////
module UCC_TOP
	(
	
    //global reset
    input Reset,
	
	//input clocks
	input Clk152MHz,
	input Clk9_5MHz,
	input osc_clk, //200MHz clock
	
	//Serial data inputs from the ADC
    input DI_0,
	input DI_1,
	input DI_2,
	input DI_3,
	
	//Data input to the UART
    input RX_IN,
	
	//Data output from the UART
    output TX_OUT,
	
	//Serial clock output for the ADC's
	output Serial_Clock,
	
	//ADC clock output
	output ADC_CLK,
	//2x adc clock frequency
	output FIFO_CLK,
	
	//Serial Data Out (unused)
	//this is for communicating to
	//the adc. For sending out 
	//programming data to the
	//adc to change its'
	//behavior
	output Data_Out,
	
	//FIFO state signals
	//fairly primitive for
	//the shift register
	//both are just inversions
	//of each other.
	//but when the FIFO is
	//used the signals operate
	//independently
	//indicates buffer
	//is full
    output Is_Full,
	
	//indicates that buffer
	//is empty
    output Is_Empty,
	//////////////////////////
	
	//debugging signals that put out a 2 hertz and 1 hertz
	//square waves respectively
	output heart2,
	output HeartBeat,  ///connect to an led and connect ADC_CLK for full speed
	
	
	//Output to indicate to the MaxMin how many ADC's are in operation
	output [3:0] ChanNum,
	
	//parallel ports for debugging and MaxMin Detector
	//basically puts out the parallel data from the
	//adc's
	output reg [15:0] EMIFPORT0,
	output reg [15:0] EMIFPORT1,
	output reg [15:0] EMIFPORT2,
	output reg [15:0] EMIFPORT3
    );
	 

//////////////////Interconnect Wires:
//These are the wires used to 
//interconnect the various wires

	//Busses:
		//ADC in wires
		wire [15:0] ADC0_W;
		wire [15:0] ADC1_W;
		wire [15:0] ADC2_W;
		wire [15:0] ADC3_W;
		
		//Character wires
		wire [7:0] CharO_UART;
		wire [7:0] RX_CharIn;
		
		//Wires for ADC selection numbers
		wire [3:0] Channel_Num;
		
		//Sequence length selection wire
		wire [1:0] Seq_Len;
		
		//clock speed selection wire
		wire [7:0] Clock_Spd;
		
	//Wires:
		//input wires?
		//wire Data_In0;
	
		//clocking wires
		//wire osc_clk;		//internal oscillator clock
		//wire Fast_Clk;	//400-304MHz clock
		//wire Clk152MHz;	//RX clock currently set at 76.8 MHz
		//wire Clk9_5MHz;	//TX clock currently set at 4.8 MHz
		
		//RX subgroup wires
		wire ADC_Clk;
		wire FIFO_Clk;
		wire Sam_State;
		wire Cmd_Load;
		//wire Cmd_Reset;
		wire Load;
		//wire Receive_In;
		wire ULD_RX_Data;
		wire RX_Reset;
		wire RX_Empty;
		wire Global_Reset;
		wire S_Load;
		
		//TX subgroup wires
		//wire S_Latch_Out;
		//wire S_Reset;
		wire TX_Reset;
		wire Latch_Out;
		wire LD_TX_Data;
		wire TX_Empty;
		//wire Transmit_Out;
		//wire LD_TX_Data_S;
		wire S_Latch_In;
		//wire Latch_In;
		wire xper_uart;

/////////////////////////////////////
	//critical part here, this bypasses the TX_FSM to make transmitting characters to the 
	//UART one clock cycle faster.
	 assign xper_uart = TX_Empty & ~Is_Empty ;
	 
	 
	 //assignment for the par output port. Experimental!
	 //Seems to work at up to 126K samples/seconds, beyond that
	 //my buffer overflows for the logic analyzer
	 always@(posedge ADC_CLK)
	 begin
		EMIFPORT0  <= ADC0_W;
		EMIFPORT1  <= ADC1_W;
		EMIFPORT2  <= ADC2_W;
		EMIFPORT3  <= ADC3_W;
	 end
	 
	 //assign statements
	 assign Global_Reset	= Reset;
	 assign ADC_CLK			= ADC_Clk;
	 assign FIFO_CLK		= FIFO_Clk; 
	 assign ChanNum			= Channel_Num; 
	 
	 
//////////////////Module instantiations:

	 //internal oscillator
	 // Internal Oscillator
	 // defparam OSCH_inst.NOM_FREQ = "2.08";// This is the default frequency
	 /*
		defparam OSCH_inst.NOM_FREQ = "133.0";
		OSCH OSCH_inst( .STDBY(1'b0), // 0=Enabled, 1=Disabled
		// also Disabled with Bandgap=OFF
		.OSC(osc_clk),
		.SEDSTDBY()); 	// this signal is not required if not
						// using SED 
	 */
	 
	 //pll's
		//400MHz pll
		/* Verilog module instantiation template generated by SCUBA Diamond (64-bit) 3.10.0.111.2 */
		/* Module Version: 5.7 */
		/* Tue Jun 26 09:42:05 2018 */
		/* parameterized module instance */
		//pll1 p1 (.CLKI( osc_clk), .CLKOP(Fast_Clk ));  
		
		//UART clock pll
		/* parameterized module instance */
		//pll2 p2 (.CLKI( osc_clk), .CLKOP(Clk152MHz ), .CLKOS(Clk9_5MHz)); 
		

	 //Synchronizers:
		//Synchronizer for CBF latchout flag
		/*
		bit_synchro S1 (
		.clkA(CLK9_5MHz), 
		.flagA(Latch_Out), 
		.clkB(FIFO_Clk), 
		.flagOut_B(S_Latch_Out)
		); */
		
		//Synchronizer for CBF latchIn
		bit_synchro S2 (
		.clkA(FIFO_Clk),  //////////////////////////change to for full speed: FIFO_Clk
		.flagA(ADC_Clk),         ///////////////change to for full speed: ADC_Clk 
		.clkB(Clk9_5MHz), 
		.reset(Global_Reset),
		.flagOut_B(S_Latch_In)
		);
		
	 
		//Synchronizer for UART ld_tx_data flag
		/*
		bit_synchro S3 (
		.clkA(Clk9_5MHz), 
		.flagA(LD_RX_Data_S), 
		.clkB(Clk152MHz), 
		.flagOut_B(LD_TX_Data)
		); */
		
		//Synchronizer for cmd_module load flag
		bit_synchro S4 (
		.clkA(Clk152MHz), 
		.flagA(S_Load), 
		.clkB(osc_clk), 
		.reset(Global_Reset),
		.flagOut_B(Load)
		);
	 
		/*
		//Synchronizer for cmd_reset flag
		bit_synchro S5 (
		.clkA(Clk152MHz), 
		.flagA(RX_Reset), 
		.clkB(Fast_Clk), 
		.flagOut_B(Cmd_Reset)
		);
		*/
		
		/*
		//Synchronizer for CBF LatchIn
		bit_synchro S6 (
		.clkA(),  //Connect to FIFO_clk?
		.flagA(), //Connect to SPI_Par Latch Out signal  
		.clkB(),  //Connect to 
		.flagOut_B(CBF_LatchIn)
		); */
	 
	 //Finite State Machines:
		//TX_FSM
		//at this point I think that this only handles Resets.
		TX_FSM F1 (
		.clk(Clk9_5MHz), 
		.global_reset(Global_Reset), 
		.is_empty(Is_Empty), 
		.tx_empty(TX_Empty), 
		.reset(TX_Reset), 
		.ld_tx_data(LD_TX_Data), 
		.latch_out(Latch_Out)
		);
		
		//RX_FSM
		//handles receiving data from the
		//uart and sends it to the command
		//module
		RX_FSM F2 (
		.clk(Clk152MHz), 
		.global_reset(Global_Reset), 
		.rx_empty(RX_Empty), 
		.uld_rx_data(ULD_RX_Data), 
		.load(S_Load), 
		.rx_reset(RX_Reset)
		);
	 
	 //UART
	 //sends and recieved data 
	 //at 9.5MBaud. uses two
	 //stop bits for sending.
	 //not written by me
		uart G1 (
		.reset(RX_Reset), 
		.txclk(Clk9_5MHz), 
		.ld_tx_data(xper_uart),  
		.tx_data(CharO_UART), 
		.tx_enable(1'b1), 
		.tx_out(TX_OUT), 
		.tx_empty(TX_Empty), 
		.rxclk(Clk152MHz), 
		.uld_rx_data(ULD_RX_Data), 
		.rx_data(RX_CharIn), 
		.rx_enable(1'b1), 
		.rx_in(RX_IN), 
		.rx_empty(RX_Empty)
		); 
	 
	 //Command Module
	 //receives from the UART
	 //and tells the clock
	 //module, ADC's, and
	 //data buffer what
	 //to do
		command_module G3 (
		.charIn(RX_CharIn), 
		.load(Load), 
		.clk_in(osc_clk), 
		.reset(RX_Reset), 
		.clkspd(Clock_Spd), 
		.seq_len(Seq_Len), 
		.sample_state(Sam_State), 
		.channel_num(Channel_Num), 
		.load_out(Cmd_Load)
		);
		
	 //Multispeed_clk Module
	 //programmable module responsible for 
	 //multiple clock speeds, and total
	 //sample counts
		multiSpeedClock G4 (
		.seq_len(Seq_Len), 
		.sample_state(Sam_State), 
		.clock_spd(Clock_Spd), 
		.clk_in(osc_clk), 
		.reset(RX_Reset), 
		.load(Cmd_Load), 
		.FIFO_clk(FIFO_Clk), 
		.ADC_clk(ADC_Clk)
		);
	 
	 //Converter Buffer Package (CBF)
	 //responsible for converting raw
	 //hex data to ascii. Also stores
	 //the data from up to four adc's,
	 //inserts comma's between samples
	 //and feeds said data to the UART
	 //a FIFO can be dropped in place
	 //instead of the Shift-Register
	 //and there is one built in the
	 //file list
		converterBufferPackage G2 (
		.sixteenBitInput0(ADC0_W), 
		.sixteenBitInput1(ADC1_W), 
		.sixteenBitInput2(ADC2_W), 
		.sixteenBitInput3(ADC3_W), 
		.charOut(CharO_UART), 
		.isFull(Is_Full), 
		.isEmpty(Is_Empty), 
		.latchIn(S_Latch_In), 
		.latchOut(xper_uart), 
		.reset(TX_Reset), 
		.clk(Clk9_5MHz), 
		.sel(Channel_Num)
		);
		
	//SPI to Parallel interface
	//Connects ADC's to CBP
		SPI_PAR SPI1(
			//inputs
			.DI0(DI_0),			//SPI input for first shift register
			.DI1(DI_1),				//SPI input for second shift register
			.DI2(DI_2),				//SPI input for third shift register
			.DI3(DI_3),				//SPI input for fourth shift register
			.ADC_CLK(ADC_Clk),    //Change to for full speed: ADC_Clk		//Falling edge clock that starts the process
			.F_CLK(osc_clk),		//fast clock (400MHz) that controls the shift registers
									//attempting slower clk instead 4.8MHz
						
			.RESET(Global_Reset),	//synchronous reset that clears everything
	
			//outputs
			.ADC_DATA0(ADC0_W), 	//four 16 bit data outputs to go to the fifo
			.ADC_DATA1(ADC1_W),
			.ADC_DATA2(ADC2_W),
			.ADC_DATA3(ADC3_W),
			.SCLK(Serial_Clock), 	//clock that controlls the shift registers, in the ADC and internally
			.DO(Data_Out)			//Data output, unused currently, set to 0
		); 
		
	//heartbeat generation clkdiv
	//can be removed. Mainly developed 
	//for debugging purposes
		heartdiv Sahrang(
			.clk_in(osc_clk),
			.reset(Global_Reset),
			.clk2(heart2),
			.clk_out(HeartBeat) 
		);
		
		/*
		//divide by 14 clock divider to make
		// the 9.5MHz clk for tx
		clk14_div div1(
			.clk_in(osc_clk),
			.reset(Global_Reset),
			.clk_out(Clk9_5MHz) 
		); */
		
////////////////////////////////////////
endmodule



//heartbeat generation clkdiv
//1Hz
//used for debugging. Can be 
//removed
module heartdiv(
    input clk_in,
	 input reset,
	 output reg clk2,
    output reg clk_out
    );
	 
	 reg [25:0] div ;
	 
	 
	 
	 always@(posedge clk_in)
	 begin
	 
		if(reset == 1)
		begin
			clk_out 	<=0;
			clk2		<=0;
			div			<=0;
		end
	 
		else
			div 		<= div + 1;
			
			if(div == 33_250_000)
			begin
				clk2		<= ~clk2;
			end
			
			if(div == 66_500_000) //back to one second period
			begin
				clk_out 	<= ~clk_out;
				clk2		<= ~clk2;
				div			<= 0;
			end
	 end


endmodule

/*
module clk14_div(input clk_in,
	 input reset,
    output reg clk_out =0
    );
	 
	 
	 //initial setting of the output to 0 so the simulator
	 //works. Please work simulator
	 //need to get rid of this, while ensuring reset 
	 //zeroes everything
	 reg [2:0] div=7;
	 
	 always@(posedge clk_in )
	 begin
	 
		if(reset == 1 && div ==7)
		begin
			clk_out		<= 0;
			div			<= 0;
			
		end
	 
		else
		begin
			div 		<= div + 4'b0001;
			if(div == 6)
			begin
				clk_out 	<= ~clk_out;
				div			<=0;
			end
		end
	 end
	 
	 
endmodule */