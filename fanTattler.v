`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    16:58:49 08/23/2017 
// Design Name: 
// Module Name:    fanTattler 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//	This is the module responsible for reporting if the fan is spinning too slowly
//		according to the fan specifications the fan tachometer creates a square pulse 
//		wave at a frequency between 133.33 and 300 Hz
//		The module uses a 16 bit counter driven by a 6.25MHz clock to measure the
//		fan period, the averager continuously averages 8 consecutive periods,
//		if the average period rises above the minimum threshold (7.5ms or 46875 counter count)
//		then the comparator outputs a zero, if it is faster than that then it reports a one
//
//		Care must be taken when implementing on a physical FPGA that all the correct registers
//			initialized to max value. This is junk data that fills the system to represent the 
//			fan starting at 0 velocity and thus a really high tachometer period. If these registers
//			are set to 0 instead then the comparator will think that the fan is violating the
//			characteristic velocity of the universe and will thus output a 1 for about 12 fan cycles
//			while the fan is trying to get up to speed.
//			
//			So, better to have the registers start out with ones than zeros. And that's why many of these
//				registers are preset to magic numbers that are really just ones.			
//
//			The Special Relativity Stasi will also give your fan hefty fines for going back in time
//////////////////////////////////////////////////////////////////////////////////
module fanTattler(
    input fanInput,
    input clk,
    output out
    );

//this wire connects the counter to the averager
wire [15:0] counterPeriod;

//this wire connects the AveEn of the counter to the clock of the averager
wire averagerClock;

//this wire connects the averaged period of the averager to the comparator
wire [15:0] averagedPeriod;

//This module is the averager, it takes in
//8 periods and averages them
Averager U0(
    .Din(counterPeriod),
    .En(averagerClock),
    .aveOut(averagedPeriod)
    );

//this module is the 16 bit timer and clocks the averager
timer16 U1(
    .fanInput(fanInput),
    .clk(clk),
	 .reset(1'b0),
    .timeOut(counterPeriod),
    .AveEn(averagerClock)
    );

//this module is the comparator	 
comparator U2(
    .period(averagedPeriod),
    .comOut(out)
    );



endmodule
