`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:38:03 08/24/2017 
// Design Name: 
// Module Name:    topModule 
// Project Name: 	 OverArchitecture
// Target Devices: 
// Tool versions: 
// Description: 
//	This module contains the overarching module that holds the fanTattler, UART
//		controlled ADC, and eventually the MaxMin surveyer. Several modules
//		inside contain dependencies based on the the input frequency of the clock
//		the specific parameters shall be labeled in comments and the modules shall
//		be labeled here:
//			multispeedclock.v
//			MaxMin.v
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module topModule(
		input fourtyMHzClk,
		input reset,
		
		//IO for the fanComparatorWrapper
		input tch0,
		input tch1,
		input tch2,
		output cmp0,
		output cmp1,
		output cmp2,
		
		
		
		//IO for the UCC
		input DI0,
		input DI1,
		input DI2,
		input DI3,
		output serial_clk,
		output adc_clk,
		output data_out,
		
		input rx_in,
		output tx_out,
		
		output is_full,
		output is_empty,
		output heart1,
		output heart2,
		
		//dummy load. remove this in the final product
		//only here to prevent synth from optimizing out
		//the max min
		output [15:0] xormaxmin
		
		
	);
	
	//clock wires
	wire Clk152MHz;
	wire Clk9_5MHz;
	wire Clk25MHz;
	wire osc_clk;
	wire fifo_clk;
	
	
	
	//other wires
	wire [3:0] ChnNum;
	wire adc_clk_pulse;
	
	//Various Data out wires
	//IO for the MaxMin
		wire [15:0] Max0;
		wire [15:0] Max1;
		wire [15:0] Max2;
		wire [15:0] Max3;
		
		wire [15:0] Min0;
		wire [15:0] Min1;
		wire [15:0] Min2;
		wire [15:0] Min3;
	
		wire [15:0] EMIF0;
		wire [15:0] EMIF1;
		wire [15:0] EMIF2;
		wire [15:0] EMIF3; 
		
		//test output to keep it from being optimized out
		//by the synthesizer
		assign xormaxmin = Max0^Max1^Max2^Max3^Min0^Min1^Min2^Min3;
	
	 //module for both PLL's
	IPGroup p12 (.p1_CLKI(fourtyMHzClk), .p1_CLKOP(osc_clk), .p1_CLKOS(Clk25MHz), .p2_CLKI(fourtyMHzClk), .p2_CLKOP(Clk152MHz), .p2_CLKOS(Clk9_5MHz)) /* synthesis sbp_module=true */ ;
	
	//module that takes in the tachometers from the fans and sends out an alert if
	//the fan speed falls below a certain preset value
	fanComparatorWrapper fCW1(
     .tach0(tch0),
     .tach1(tch1),
     .tach2(tch2),
     .clk25MHz(Clk25MHz),
     .comp0(cmp0),
     .comp1(cmp1),
     .comp2(cmp2) 
    );
	
	
    
	 //UART Controlled Clock
	UCC_TOP UCC1(
    //global reset
    .Reset(reset),
	
	//Serial data inputs from the ADC
    .DI_0(DI0),
	.DI_1(DI1),
	.DI_2(DI2),
	.DI_3(DI3),
	
	//Data input to the UART
    .RX_IN(rx_in),
	
	//Data output from the UART
    .TX_OUT(tx_out),
	
	//Serial clock output for the ADC's
	.Serial_Clock(serial_clk),
	
	.Clk152MHz(Clk152MHz),
	.Clk9_5MHz(Clk9_5MHz),
	.osc_clk(osc_clk),
	
	//ADC clock output
	.ADC_CLK(adc_clk),
	
	//fifo clock output
	.FIFO_CLK(fifo_clk),   
	
	//Serial Data Out (unused)
	.Data_Out(data_out),
	
	//FIFO state signals
    .Is_Full(is_full),
    .Is_Empty(is_empty),
	.heart2(heart2),
	.HeartBeat(heart1),  ///connect to an led and connect ADC_CLK for full speed
	.ChanNum(ChnNum),
	.EMIFPORT0(EMIF0), //[15:0]
	.EMIFPORT1(EMIF1),
	.EMIFPORT2(EMIF2),
	.EMIFPORT3(EMIF3)
    );
	
	//Synchronizer for the MaxMin Modules
	bit_synchro S1 (
		.clkA(fifo_clk), 
		.flagA(adc_clk), 
		.clkB(Clk9_5MHz), 
		.reset(reset),
		.flagOut_B(adc_clk_pulse)
		);
	
	//Max min reporters. One for each ADC.
	MaxMin M0(
		.reset(reset),
		.data(EMIF0),
		.ADC_CLK(adc_clk_pulse),
		.clk(Clk9_5MHz),
		.Persistence(1'b0),
		.Enable(ChnNum[0]),
		
		.Max(Max0),
		.Min(Min0)
	);
	
	MaxMin M1(
		.reset(reset),
		.data(EMIF1),
		.ADC_CLK(adc_clk_pulse),
		.clk(Clk9_5MHz),
		.Persistence(1'b0),
		.Enable(ChnNum[1]),
		
		.Max(Max1),
		.Min(Min1)
	);
	
	MaxMin M2(
		.reset(reset),
		.data(EMIF2),
		.ADC_CLK(adc_clk_pulse),
		.clk(Clk9_5MHz),
		.Persistence(1'b0),
		.Enable(ChnNum[2]),
		
		.Max(Max2),
		.Min(Min2)
	);
	
	MaxMin M3(
		.reset(reset),
		.data(EMIF3),
		.ADC_CLK(adc_clk_pulse),
		.clk(Clk9_5MHz),
		.Persistence(1'b0),
		.Enable(ChnNum[3]),
		
		.Max(Max3),
		.Min(Min3)
	);


endmodule


