`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:01:48 05/23/2018
// Design Name:   fourfourmultiplexor
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/multimultiplexor_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: fourfourmultiplexor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module multimultiplexor_tb;

	// Inputs
	reg [31:0] in0;
	reg [31:0] in1;
	reg [31:0] in2;
	reg [31:0] in3;
	reg [3:0] sel;

	// Outputs
	wire [31:0] out0;
	wire [31:0] out1;
	wire [31:0] out2;
	wire [31:0] out3;
	wire [1:0] num;

	// Instantiate the Unit Under Test (UUT)
	fourfourmultiplexor uut (
		.in0(in0), 
		.in1(in1), 
		.in2(in2), 
		.in3(in3), 
		.sel(sel), 
		.out0(out0), 
		.out1(out1), 
		.out2(out2), 
		.out3(out3), 
		.num(num)
	);

	initial begin
		// Initialize Inputs
		in0 = 0;
		in1 = 0;
		in2 = 0;
		in3 = 0;
		sel = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		
		in0 = 32'hA;
		in1 = 32'hB;
		in2 = 32'hC;
		in3 = 32'hD;
		sel = 0;
		
		#5;
		sel = 0;
		
		#5;
		sel = 1;
		
		#5;
		sel = 2;
		
		#5;
		sel = 3;
		
		#5;
		sel = 4;
		
		#5;
		sel = 5;
		
		#5;
		sel = 6;
		
		#5;
		sel = 7;
		
		#5;
		sel = 8;
		
		#5;
		sel = 9;
		
		#5;
		sel = 10;
		
		#5;
		sel = 11;
		
		#5;
		sel = 12;
		
		#5;
		sel = 13;
		
		#5;
		sel = 14;
		
		#5;
		sel = 15;
		
		#5;
		sel = 0;

	end
      
endmodule

