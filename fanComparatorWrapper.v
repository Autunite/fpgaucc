`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:38:03 08/24/2017 
// Design Name: 
// Module Name:    fanComparatorWrapper 
// Project Name: 	 fandsp
// Target Devices: 
// Tool versions: xilinx ISE 14.7
// Description: 
//	This project eliminates a daughter board from the LC control board
//	It works by using 6 pins (3 in, 3out) off of the onboard fpga
//		to do simple fan speed analysis and averaging. Each tachometer is connected
//		to a 16 bit counter that measures the fan period. The counter outputs
//		to a SIPO shift register that gathers 8 sequential data values
//		the data values are all added together and divided by 8 (shift <<3)
//		This averaged value gets outputted to the comparator, if the period
//		is greater than the equivalent time represented by 46875 counter ticks
//		then the comparator outputs 0, else it outputs 1.
//
//		Because of the nature of shift registers, it takes 12 successive matching fan tachometer waves
//			for the comparator to see the average become the value of the repeated waves. This is because while
//			the averager only averages 8 waves there are several other registers scattered about the design
//			that the wave has to propogate through. 
//
//		
//		
//		NOTE: This digital system REQUIRES a 25MHz clock to work properly.
//		Other clocks can work, but then all of the values for the comparator
//		and bus widths would probably have to change.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fanComparatorWrapper(
    input tach0,
    input tach1,
    input tach2,
    input clk25MHz,
    output comp0,
    output comp1,
    output comp2
    );

wire sixPointTwoFiveClk;

	//three fantattlers,
	//one for each fan
	fanTattler U0(
	.fanInput(tach0),
	.clk(sixPointTwoFiveClk),
	.out(comp0)
	);
	 
	fanTattler U1(
	.fanInput(tach1),
	.clk(sixPointTwoFiveClk),
	.out(comp1)
	);
	 
	fanTattler U2(
	.fanInput(tach2),
	.clk(sixPointTwoFiveClk),
	.out(comp2)
	);

	//clock divider to clock down
	//the clock from 25MHz to
	//6.25MHz
	clockdiv25to6point25MHz U3(
    .twentyFiveMHzClk(clk25MHz),
    .sixPointTwoFiveClk(sixPointTwoFiveClk)
    );

endmodule
