`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:13:27 06/20/2018 
// Design Name: 
// Module Name:    TX_FSM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This is the finite state machine that controls the transmitter
//		of the UART, it is responsible for sending out a reset to to the CBF when
//		the global reset is asserted and for loading data from the CBF into the 
//		UART
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////



module TX_FSM(

	

	//Input clock for the FSM, uses 9.5MHz which is conveniently 1/16 of 152MHz
    input clk,
	 
	//Global reset input
    input global_reset,
	 
	//Signal from the CBF that tell the FSM that it is empty and thus cannot
	//send more data
    input is_empty,
	 
	//Input that tells the FSM that the UART is empty and thus is ready to receive
	//another character
    input tx_empty,
	 
	//Output controlled by the global reset. Resets the CBF
    output reg reset,
	 
	//Output that loads data into into the UART
    output reg ld_tx_data,
	 
	//Output that latches data out from the CBF
    output reg latch_out
    );
	 
	 reg [1:0] STATE; 
	 reg syncer;		//the fsm is too fast for the uart so I am adding a one gate delay
						// to give time for the uart empty signal to propogate
	 parameter RESET = 2'b00;
	 parameter IDLE = 2'b01;
	 parameter LATCH_CBF = 2'b10;
	 parameter LATCH_UART = 2'b11;
	 
always@(posedge clk)
begin
	
	if(global_reset)
		begin
			STATE <= RESET;
		end
	else
		begin
			case(STATE)
				default :
				begin
					STATE 		<= IDLE;
					reset		<= 0;
					ld_tx_data	<= 0;
					latch_out	<= 0;
					syncer		<= 0;
				end
		
				RESET : ///0
				begin
					STATE <= IDLE;
					reset		<= 1;
					ld_tx_data	<= 0;
					latch_out	<= 0;
					syncer		<= 0;
				end
		
				IDLE : //1
				begin
					
					
					if(is_empty != 1 && tx_empty == 1)// && syncer == 0)
						begin
							STATE <= LATCH_UART;
							reset		<= 0;
							ld_tx_data	<= 1;
							latch_out	<= 1;
							syncer		<= 0;
						end
					
					else
						begin
							STATE <= IDLE;
							reset		<= 0;
							ld_tx_data	<= 0;
							latch_out	<= 0;
							syncer		<= 0;
						end 
						
				end
		
				LATCH_CBF : //2
				begin
					STATE <= IDLE;
					reset		<= 0;
					ld_tx_data	<= 0;
					latch_out	<= 1; 
					syncer		<= 0;
				end
		
				LATCH_UART : //3
				begin
					if(syncer == 1)
					begin
						STATE 		<= IDLE; //LATCH_CBF; ////////////
						reset		<= 0;
						ld_tx_data	<= 0;
						latch_out	<= 0;
						syncer		<= 0;
					end
					
					else
					begin
						STATE 		<= LATCH_UART; //LATCH_CBF; ////////////
						reset		<= 0;
						ld_tx_data	<= 0;
						latch_out	<= 0;
						syncer		<= 1;
					end
				end
		
			endcase
		end 
	
	/*
	//consider merging is_empty and tx_empty
	 casez({global_reset,is_empty,tx_empty})
		
		//case that handles resets
		3'b1??	:
		begin
			reset 		<= 1;
			ld_tx_data	<= 0;
			latch_out	<= 0;
		end
		
		//If reset isn't asserted, CBF isn't full
		//and UART tx is empty then latch out a 
		//character
		3'b001	:
		begin
			reset 		<= 0;
			ld_tx_data	<= 1;
			latch_out	<= 1;
		end
		
		//default case, basically when nothing
		//is happening set all the outputs to 
		//0
		default :
		begin
			reset 		<= 0;
			ld_tx_data	<= 0;
			latch_out	<= 0;
		end
	 endcase
	 */
end

endmodule
