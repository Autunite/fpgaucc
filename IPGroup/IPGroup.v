`timescale 1ns / 1ps
/* synthesis translate_off*/
`define SBP_SIMULATION
/* synthesis translate_on*/
`ifndef SBP_SIMULATION
`define SBP_SYNTHESIS
`endif

//
// Verific Verilog Description of module IPGroup
//
module IPGroup (p1_CLKI, p1_CLKOP, p1_CLKOS, p2_CLKI, p2_CLKOP, p2_CLKOS) /* synthesis sbp_module=true */ ;
    input p1_CLKI;
    output p1_CLKOP;
    output p1_CLKOS;
    input p2_CLKI;
    output p2_CLKOP;
    output p2_CLKOS;
    
    
    p1 p1_inst (.CLKI(p1_CLKI), .CLKOP(p1_CLKOP), .CLKOS(p1_CLKOS));
    p2 p2_inst (.CLKI(p2_CLKI), .CLKOP(p2_CLKOP), .CLKOS(p2_CLKOS));
    
endmodule

