--VHDL instantiation template

component IPGroup is
    port (p1_CLKI: in std_logic;
        p1_CLKOP: out std_logic;
        p1_CLKOS: out std_logic;
        p2_CLKI: in std_logic;
        p2_CLKOP: out std_logic;
        p2_CLKOS: out std_logic
    );
    
end component IPGroup; -- sbp_module=true 
_inst: IPGroup port map (p2_CLKI => __,p2_CLKOP => __,p2_CLKOS => __,p1_CLKI => __,
            p1_CLKOP => __,p1_CLKOS => __);
