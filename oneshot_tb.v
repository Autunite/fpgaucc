`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:06:50 05/29/2018
// Design Name:   oneshot
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/oneshot_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: oneshot
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module oneshot_tb;

	// Inputs
	reg enable;
	reg clk;
	wire oneshot;

	// Instantiate the Unit Under Test (UUT)
	oneshot uut (
		.enable(enable), 
		.clk(clk), 
		.oneshot(oneshot)
	);

	initial begin
		// Initialize Inputs
		enable = 0;
		clk = 0;
		//oneshot = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		enable = 1;
		#40;
		enable = 0;
		#10;
		enable = 1;
		#10;
		enable = 0;
	end
      
		
		always 
       #5  clk =  ! clk;
		
endmodule

