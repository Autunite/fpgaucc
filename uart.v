`timescale 1ns / 1ps

//-----------------------------------------------------
// Design Name : uart 
// File Name   : uart.v
// Function    : Simple UART
// Coder       : Deepak Kumar Tala
//----------------------------------------------------- 

/*
	There is a bug somewhere in here causing some characters
	temporarily corrupt themselves at the end of a a tx
	transmitting a character. I will investigate.
*/
module uart (
reset          ,
txclk          ,
ld_tx_data     ,
tx_data        ,
tx_enable      ,
tx_out         ,
tx_empty       ,
rxclk          ,
uld_rx_data    ,
rx_data        ,
rx_enable      ,
rx_in          ,
rx_empty
);
// Port declarations
input        reset          ;	//	resets the tx and rx regs and the associated logi
input        txclk          ;	//	clock used to transmit data
input        ld_tx_data     ;	//	strobe used to load a byte into the transmit reg
input  [7:0] tx_data        ;	//	input of the trasnmit reg
input        tx_enable      ;	//	logic used to enable the sending of transmit data
output       tx_out         ;	//	output for the sending out data
output       tx_empty       ;	//	signal used to indicate that the transmit buffer was empty
input        rxclk          ;	//	clock used to receive data, must be 16x the bitrate
input        uld_rx_data    ;	//	strobe used to move the data in the receive register to the rx_data register
output [7:0] rx_data        ;	//	byte that holds the recieved data byte per byte
input        rx_enable      ;	//	enable that allows the receive data to listen in to incoming signals
input        rx_in          ;	// input port that receives the incoming data
output       rx_empty       ;	// signal that indicates when the rx_data reg is empty allowing it to receive another byte

// Internal Variables 
reg [7:0]    tx_reg         ;	//register that hold the data ready to send out
reg          tx_empty       ;	//reg that indicates whether the transmit data buffer is empty, ready to transmit another byte
reg          tx_over_run    ;	//signal that indicates whether too many bits are transmitted in a packet
reg [3:0]    tx_cnt         ;	//counter used for sending out data
reg          tx_out         ;	//??
reg [7:0]    rx_reg         ;	//temp buffer that holds received data
reg [7:0]    rx_data        ;	//reg that receives data from the buffer above
reg [3:0]    rx_sample_cnt  ;	//counter used for sampling received data
reg [3:0]    rx_cnt         ;	//counter that measures the inputted bits
reg          rx_frame_err   ;	//register for frame errors of some form
reg          rx_over_run    ;	//register for overunn errors of some form
reg          rx_empty       ;	//register that signals when the rx is ready to receive more data
reg          rx_d1          ;	//used for edge detection
reg          rx_d2          ;	//also used for edge detection
reg          rx_busy        ;	//indicates that the rx port is currently receiving data

// UART RX Logic
always @ (posedge rxclk or posedge reset)
if (reset) 
begin
  rx_reg        <= 0; 
  rx_data       <= 0;
  rx_sample_cnt <= 0;
  rx_cnt        <= 0;
  rx_frame_err  <= 0;
  rx_over_run   <= 0;
  rx_empty      <= 1;
  rx_d1         <= 1;
  rx_d2         <= 1;
  rx_busy       <= 0;
end 

else 
begin
  // Synchronize the asynch signal
  rx_d1 <= rx_in; //rx edge detection?
  rx_d2 <= rx_d1; //reset sets these at 1, so idle is high voltage 
  //and a falling edge indicates the start of a frame
  
  // Uload the rx data
  if (uld_rx_data) 
  begin
	//transfers data from the buffer
	//to the output register
	//marks the register as empty
    rx_data  <= rx_reg;
    rx_empty <= 1;
  end
  
  // Receive data only when rx is enabled
  if (rx_enable) 
  begin
  
    // Check if uart just received start of frame
    if (!rx_busy && !rx_d2) 
	 begin
	 //rx set as busy
      rx_busy       <= 1;
	 //sampling is incremented
      rx_sample_cnt <= 1;
	 //bit count is initialized to 0
      rx_cnt        <= 0;
    end
	 
    // Start of frame detected, Proceed with rest of data
    if (rx_busy) 
	 begin
		//increase the bitcount
       rx_sample_cnt <= rx_sample_cnt + 1;
		 
       // Logic to sample at middle of data
       if (rx_sample_cnt == 7) 
		 begin
          if ((rx_d2 == 1) && (rx_cnt == 0)) 
			 begin
            rx_busy <= 0;
          end 
			 
			 else 
			 begin
            rx_cnt <= rx_cnt + 1; 
				
            // Start storing the rx data
            if (rx_cnt > 0 && rx_cnt < 9) 
				begin
              rx_reg[rx_cnt - 1] <= rx_d2;
            end
				
            if (rx_cnt == 9) 
				begin
               rx_busy <= 0;
					
               // Check if End of frame received correctly
               if (rx_d2 == 0) 
					begin
                 rx_frame_err <= 1;
               end 
					
					else 
					begin
                 rx_empty     <= 0;
                 rx_frame_err <= 0;
					  
                 // Check if last rx data was not unloaded,
                 rx_over_run  <= (rx_empty) ? 0 : 1;
               end
            end
          end
       end 
    end 
  end
  
  if (!rx_enable) 
  begin
    rx_busy <= 0;
  end
end

// UART TX Logic
always @ (posedge txclk or posedge reset)
if (reset) begin
  tx_reg        <= 0;
  tx_empty      <= 1;
  tx_over_run   <= 0;
  tx_out        <= 1;
  tx_cnt        <= 0;
end 

else 
begin
   if (ld_tx_data) 
	begin
      if (!tx_empty) 
		begin
			tx_over_run <= 0;
		end 
		
		else 
		begin
			tx_reg   <= tx_data;
			tx_empty <= 0;
		end
	end
   if (tx_enable && !tx_empty) 
	begin
     tx_cnt <= tx_cnt + 1;
     if (tx_cnt == 0) 
	  begin
       tx_out <= 0;
     end
	  
     if (tx_cnt > 0 && tx_cnt < 9) 
	  begin
        tx_out <= tx_reg[tx_cnt -1];
		//if(tx_cnt == 8) 		////////////
		//begin	/////////
		//	tx_empty <= 1;	/////////
		//end //////////
     end
	  
     if (tx_cnt == 9) 
	  begin
       tx_out <= 1;
       tx_cnt <= 0;
       tx_empty <= 1;
     end
	  
   end
	
   if (!tx_enable) 
	begin
     tx_cnt <= 0;
   end
	
end

endmodule
