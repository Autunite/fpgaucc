//-----------------------------------------------------
// Design Name : uart_simple
// File Name   : uart_top.v
// Function    : Simple UART
// Coder       : John Recchio
//-----------------------------------------------------

/*
	5/22/2018
	I am taking over this part of the project to build a module that takes up
	to four 16 bit values, selects from them and transmits over the UART those values
	in ASCII encoding and adds an ASCII comma to the end. Later I will add in an functionality
	so that the UART can take in commands that changes its' behavior. My current task is
	to see what has been built and see what needs to be added in. I'll be adding comments into the
	code to make it more readable to my eyes
	
	OSCH_inst is not showing up, assuming that it is a hardware specific feature, will probably
	replace with a clock generator of some form
	
*/

module uart_top (

//resets the uart in some manner, probably clears the registers
	reset          ,
	
//transmission output to the uart
	tx_out         ,
	
//data line that presumably moves data in the shift register to a buffer register
//assuming triggers on high or rising edge
	uld_rx_data    ,
	
// receive transmission line
	rx_in          ,
	
// data line that represents whether the receive buffer is empty
// assuming a high on this line represents an empty buffer
	rx_empty,
	
//tied and inverted to the input reset i.e. reset =1 then reset_status = 0
	reset_status,
	
//clock input, may be modified to fit specified baud rates
//133 MHZ
	osc_clk
);

// Port declarations
input        reset          ;
input        uld_rx_data    ;
input        rx_in          ;
input			 osc_clk			 ;

output       tx_out         ;

output       rx_empty       ;
output		reset_status;

//Internal Registers
	wire 		osch_clk; 		//System clock
	reg 		rx_clk;			//UART rx clock, must be 16x faster than tx clock
	reg			tx_clk;			//UART tx clock
	reg [11:0]	tx_count;	//assuming counter for controlling the transmission rate
	reg [7:0]	rx_count;	//assuming counter for controlling sample rate

	reg  [7:0]	tx_data;		//8 bit register for output
	wire [7:0]	rx_data;		//8 bit wire for received data (why wire over reg?)
	reg			tx_enable;	//control line for enabling trasmissions, controlled by reset
	reg			rx_enable;	//control line for enabling receiving of data, controlled by reset
	reg			ld_tx_data;	//assuming control line for loading trasmission data into shift register
									//for output?
	
	wire       	tx_empty;	//control line representing whether the trasmission line is empty
	
	
	assign reset_status = ~reset; //aha reset status is simply tied and inverted from the input line reset
	
	//rx tx control lines are set to 0 if reset is stated and one if not
	always @(posedge osc_clk)
	begin
		if(reset)
		begin
			tx_enable <= 1'b0;
			rx_enable <= 1'b0;
		end else begin
			tx_enable <= 1'b1;
			rx_enable <= 1'b1;
		end
	end

	//this area defines the clock, will comment it out and put in a clock
	//input instead
	/*
	defparam OSCH_inst.NOM_FREQ = "133.00";
	OSCH OSCH_inst( .STDBY(1'b0), .OSC(osc_clk),.SEDSTDBY());
	*/



	// for baud rate of 115200, 
	// tx_count = 2.08M/115200 ~=330
	// rx_count = tx_counts/16 ~=21
	// baud of 9600bps, tx_count = 3958
	// rx_count = 247
	always @(posedge osc_clk) 
	begin
		tx_count <= tx_count + 12'd1;
		rx_count <= rx_count + 8'd1;
		
		if(tx_count == 12'd7)
		begin
			tx_count <= 0;
			tx_clk <= ~tx_clk;
		end
		if(rx_count == 8'd67)
		begin
			rx_count <= 0;
			rx_clk <= ~rx_clk;
		end
	end


	
	reg tx_clk_dly;
	wire tx_clk_edge;
	assign tx_clk_edge = tx_clk & ~tx_clk_dly;
	always @(posedge osc_clk)
	begin
		tx_clk_dly <= tx_clk;
	end


	parameter 	STATE0 = 2'b00,
				STATE1 = 2'b01,
				STATE2 = 2'b10,
				STATE3 = 2'b11;




	parameter 	ZERO 		= 8'b00110000,
					ONE		= 8'b00110001,
					TWO		= 8'b00110010,
					THREE		= 8'b00110011,
					FOUR		= 8'b00110100,
					FIVE		= 8'b00110101,
					SIX		= 8'b00110110,
					SEVEN		= 8'b00110111,
					EIGHT		= 8'b00111000,
					NINE		= 8'b00111001,
					A			= 8'b01000001,
					B			= 8'b01000010,
					C			= 8'b01000011,
					D			= 8'b01000100,
					E			= 8'b01000101,
					F			= 8'b01000110,
					COMMA		= 8'b00101100,
					LF			= 8'b00001101,
					CR 		= 8'b00001010;
				


	reg [1:0] currentState, nextState;
	
	
	
	
	always @(posedge osc_clk) 
	begin
		if(reset)
		begin
			currentState <= STATE0;
		end else begin
			currentState <= nextState;
			tx_data <= 8'b01000010;
		end
	end
	
	always @(*) 
	begin
		nextState = currentState;
		case (currentState)
			STATE0: 
			begin 
				ld_tx_data = 1'b0;
				if(tx_empty & tx_clk_edge)
				begin
					nextState = STATE1;
					ld_tx_data = 1'b1;
				end
			end
			STATE1:
			begin
				ld_tx_data = 1'b1;
				if(~tx_empty & tx_clk_edge)
				begin
					nextState = STATE0;
				end
			end
		endcase
	end
	



	uart U1 (
		.reset		(reset)			,
		.txclk 		(tx_clk)		,
		.ld_tx_data (ld_tx_data)   	,
		.tx_data    (tx_data)    	,
		.tx_enable  (tx_enable)    	,
		.tx_out     (tx_out)    	,
		.tx_empty   (tx_empty)    	,
		.rxclk      (rxclk)    		,
		.uld_rx_data(uld_rx_data)   ,
		.rx_data    (rx_data)    	,
		.rx_enable	(rx_enable)     ,
		.rx_in		(rx_in)         ,
		.rx_empty	(rx_empty)
	);
	 
 converterBufferPackage CBP1 (
 
 );

endmodule
