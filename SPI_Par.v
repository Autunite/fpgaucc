`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    9:28 6/27/2018 
// Design Name: 
// Module Name:    UCC_TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This module directly interfaces with the ADC's. One of them will
//		be used to talk to the four ADC's. It works as follows:
//			1. Waits for a falling edge on the ADC_CLK input, which it sends out
//				to the ADC, using CS pin.
//			2. A four bit counter runs and sends out sixteen rising edges from 
//				the Fast clock to the ADC. With each fast clock rising edge
//				the module latches in data into the first internal 16 bit
//				register. Data comes in MSb first so care must be taken
//				so that the data is arranged properly.
//			3. The latched in data is copied to the 16 bit output register.
//			4. 1000ns is waited before moving back to step one
//			5. Reset clears all registers and moves everything back to step
//				one
//
//		Ignore the above. This module shouldn't need such a description.
//			its' just a trumped up shift register. Operation should be 
//			relatively simple
//
// Dependencies: uses oneshot.v and clockdiv16.v
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: I followed reccomended procedures as stated by white papers
//	they were a glitchy pile of manure. So I commented out all of the code and
//	decided to start anew. This time properly breaking down everything into its'
//	proper submodules for easier debugging operations. Wish me luck. I hope to
//	get this relatively simple module done by mid afternoon.
//
//	Revision 0.02 - Redone
//		I cleared everything. I even removed the shift register clock.
//		The external ports will be directly connected in parallel to this
//		module. So there won't be delays associated with it. 
//
//		As is it currently appears to work.
//
//	Revision 0.03 - Synchronous
//		
//
//////////////////////////////////////////////////////////////////////////////////

module SPI_PAR(
    //inputs
	input DI0,		//SPI input for first shift register
	input DI1,		//SPI input for second shift register
	input DI2,		//SPI input for third shift register
	input DI3,		//SPI input for fourth shift register
	input ADC_CLK,	//Falling edge clock that starts the process
	input F_CLK,	//fast clock (400MHz) that controls the shift registers
	input RESET,	//synchronous reset that clears everything
	
	//outputs
	output [15:0] ADC_DATA0, //four 16 bit data outputs to go to the fifo
	output [15:0] ADC_DATA1,
	output [15:0] ADC_DATA2,
	output [15:0] ADC_DATA3,
	output SCLK, //clock that controlls the shift registers, in the ADC and internally
	output DO	//Data output, unused currently, set to 0
    ); 
	
	assign DO = 1'b0;
	
	//wires
	//falling edge notifier
	wire Falling_Edge;
	
	//Shift register output enable
	wire OE;
	
	//25MHz clock generated from
	//400MHz clock
	wire SLOW_CLOCK;
	
	//wire used to reset the shift registers
	wire S_Reset;
	
	//internal wire for clocking shiftregisters
	wire SRegister_CLK;
	
	//module uses four submodules that are basically
	//shift registers
	SPISR SR0(
	.SPI_IN(DI0), 	//input to the shift registers
	.SR_LI(SRegister_CLK),	//rising edge latch in for data
	.D_EN(OE),		//Maps SR data to output
	.RESET(S_Reset),	//Asychnronous reset
	.Par_Out(ADC_DATA0)
	);
	
	SPISR SR1(
	.SPI_IN(DI1), 	//input to the shift registers
	.SR_LI(SRegister_CLK),	//rising edge latch in for data
	.D_EN(OE),		//Maps SR data to output
	.RESET(S_Reset),	//Asychnronous reset
	.Par_Out(ADC_DATA1)
	);
	
	SPISR SR2(
	.SPI_IN(DI2), 	//input to the shift registers
	.SR_LI(SRegister_CLK),	//rising edge latch in for data
	.D_EN(OE),		//Maps SR data to output
	.RESET(S_Reset),	//Asychnronous reset
	.Par_Out(ADC_DATA2)
	);
	
	SPISR SR3(
	.SPI_IN(DI3), 	//input to the shift registers
	.SR_LI(SRegister_CLK),	//rising edge latch in for data
	.D_EN(OE),		//Maps SR data to output
	.RESET(S_Reset),	//Asychnronous reset
	.Par_Out(ADC_DATA3)
	);
	
	//one shot used for detecting falling edges
	oneshot O1(
    .enable(~ADC_CLK),
    .clk(SLOW_CLOCK),
	.reset(RESET),
    .oneshot(Falling_Edge)
    );
	
	SR_CTRLER CTRL(
	.CLK(SLOW_CLOCK),				//input clock, will determine freq 
	.Falling_Edge(Falling_Edge), 	//input that starts the cycle
	.RESET(RESET),
	.SR_CLK(SRegister_CLK),			//output that controls latching the internal SR's
	.Serial_CLK(SCLK),				//output for latching the ADC's
	.SR_OE(OE),
	.SR_RESET(S_Reset)
	);
	
	/*
	clk8_div DIV1(
    .clk_in(F_CLK),
	.reset(RESET),
    .clk_out(SLOW_CLOCK)
    );
	*/
	
	//Decided to slow down the clock more
	//because of my physical test stand
	//should work with the above divider
	//but I got time to spare here so lets
	//do this for extra signal integrity
	clk16_div DIV2(
    .clk_in(F_CLK),
	.reset(RESET),
    .clk_out(SLOW_CLOCK) 
    );
	
endmodule

//this module controls the shift registers
//when it detects a falling edge on the ADC clock
//it sends out 16 rising edges to the SCLK and then
//sends a rising edge to the D_EN signal of the
//SR
//All of this must be done within 1000ns
module SR_CTRLER(
	input CLK,			//input clock, will determine freq
	input Falling_Edge, //input that starts the cycle
	input RESET,
	output reg SR_CLK,		//output that controls latching the internal SR's
	output reg Serial_CLK,	//output clock for the ADC's
	output reg SR_OE,		//output that tells SR to expose it's data to the world
	output reg SR_RESET
	);
	
	//various internal registers
		//0 is resting
		//1 is pulsing the SR_LI
		//2 is sending pulse to D_EN
		reg [1:0]  state;
		
		//counts out 16 rising edges
		reg [5:0] counter;
		//reg clken;
		
		//assign SR_CLK = (clken) ? CLK : 0;
	
	// 
	always@(posedge CLK, posedge RESET) 
		begin
			if(RESET ==1)
				begin
					state		<= 3;
					counter		<= 0;
					SR_CLK		<= 0;
					Serial_CLK	<= 0;
					SR_OE		<= 0;
					SR_RESET	<= 1;
				end
				
			else
				begin
					//if in resting state
					if(state ==0)
						begin
							//move to next state if falling edge is found
							if(Falling_Edge)
								begin
									state		<= 1;
									counter		<= 0;
									SR_CLK		<= 0;
									Serial_CLK	<= 0;
									SR_OE		<= 0;
									SR_RESET	<= 0;
								end
								
							else
								begin
									state		<= 0;
									counter		<= 0;
									SR_CLK		<= 0;
									Serial_CLK	<= 0;
									SR_OE		<= 0;
									SR_RESET	<= 0;
								end
						end
					
					//if in strobe state
					else if(state == 1)
						begin
							//if counter is 15 then next state
							if(counter == 32)
								begin
									state		<= 2;
									counter		<= 0;
									SR_CLK		<= 0;
									Serial_CLK	<= 0;
									SR_OE		<= 0;
									SR_RESET	<= 0;
								end
							else
								begin
									state		<= 1;
									counter		<= counter + 1;
									SR_CLK		<= ~SR_CLK;
									Serial_CLK	<= ~Serial_CLK;
									SR_OE		<= 0;
									SR_RESET	<= 0;
								end
						end
					//else in pulse SR_OE state
					else if(state == 2)
						begin
							//if one pulse is sent out then 
							//go back to wait state
							if(counter == 2)
								begin
									state		<= 0;
									counter		<= 0;
									SR_CLK		<= 0;
									Serial_CLK	<= 0;
									SR_OE		<= 0;
									SR_RESET	<= 0;
								end
								
							else
								begin
									state		<= 2;
									counter		<= counter + 1;
									SR_CLK		<= 0;
									Serial_CLK	<= 0;
									SR_OE		<= CLK;
									SR_RESET	<= 0;
								end
						end
						
						//state 3 reset state
						else
							begin
								state		<= 0;
								counter		<= 0;
								SR_CLK		<= 1;
								Serial_CLK	<= 0;
								SR_OE		<= 0;
								SR_RESET	<= 1;
							end
						
				end
		end
	
	
endmodule



//16 bit shift registers. Of which there will be
//four of them
module SPISR(
	input SPI_IN, 	//input to the shift registers
	input SR_LI,	//rising edge latch in for data
	input D_EN,		//Maps SR data to output
	input RESET,	//Asychnronous reset
	output reg [15:0] Par_Out
	);
	
	//reg [15:0] SR;
	
	
	always@(posedge SR_LI)//,  posedge RESET)//, posedge D_EN)
		begin
			
			//clear on reset
			if(RESET)
				begin
					//SR 		<= 0;
					Par_Out <= 0;
				end
				
			//else shift or expose
			else
				
				Par_Out <= {Par_Out[14:0],SPI_IN};
				
				/*
				begin
					if(D_EN)
						begin
							Par_Out <= SR;
							SR		<= SR;
						end
					else
						begin
							SR <= {SR[14:0],SPI_IN};
							Par_Out <= Par_Out;
						end
				end
				*/
		end
	
endmodule









/*	
	//parameters for statemachine
	localparam Reset = 3'b01;
	localparam Read =  3'b10;
	localparam Wait =  3'b11;
	
	//internal registers
	reg [4:0] rising_edge_counter;
	reg [1:0] current_state = Reset;
	reg [1:0] next_state; 
	reg [1:0] read_state;
	reg A,B,C,X,Y,Z;
	reg divReset;
	
	//Shift Registers
	reg [15:0] sr0, sr1, sr2, sr3;
	
	//internal wires
	wire clk25MHz;
	
	//sub modules
	clk16_div clkdiv(
     .clk_in(F_CLK),
	 .reset(divReset),
     .clk_out(clk25MHz) 
    );
	
	initial begin
		ADC_DATA0<=0;
		ADC_DATA1<=0;
		ADC_DATA2<=0;
		ADC_DATA3<=0;
		SCLK<=0;
		CS<=1;
		DO<=0;
	end
	
	//attempt one will be based off the fast clock
	//using a 400MHz input clock
	always@(posedge F_CLK)
	begin
		if(RESET)
		begin
				current_state 	<= Reset;
				DO				<= 0;
				A				<= ADC_CLK;
				B				<= A;
				C				<= B;
				X				<= clk25MHz;
				Y				<= X;
				Z				<= Y;
		end
			
		else
		begin
				current_state 	<= next_state;
				DO				<= 0;
				A				<= ADC_CLK;
				B				<= A;
				C				<= B;
				X				<= clk25MHz;
				Y				<= X;
				Z				<= Y;
				
				
				
		end
	end
		
		
	always@(*)
		begin
			//begin state machine
			case (current_state)
			
				//default state sends it to the reset state
				default : begin
					next_state <= Reset;
				end
			
				//reset state, or initial state
				//waits for a falling edge on ADC_CLK
				Reset : begin
				
					//reset counters and shift registers
					rising_edge_counter	<= 0;
					sr0					<= 0;
					sr1					<= 0;
					sr2					<= 0;
					sr3					<= 0;
					CS					<= 1;
					divReset			<= 0;
					SCLK				<= 0;
					read_state 			<= 0;
				
					//if falling edge is detected
					//lower CS
					if(C== 1 && B == 0) 
					begin
						CS	<= 0;
						next_state <= Read;
					end
				
					else //else keep current state
					begin
						next_state <= Reset;
					end
				
				end
			
				//read state, uses 25MHz clock to strobe
				//the ADC and the FIFO, count out 16
				//rising edges on SCLK, consider hooking 
				//up CS to fifo latch in
				Read : begin
				
					rising_edge_counter	<= 0;
					sr0					<= 0;
					sr1					<= 0;
					sr2					<= 0;
					sr3					<= 0;
					CS					<= 0;
					SCLK				<= 0;
					divReset			<= 1;
				
					case(read_state)
						default:
						begin
							read_state <= 0;
						end
					
						//delay
						0 : begin
							divReset <= 0;
							//detect rising edge on 25MHz clk
							if(Z==0 && Y == 1)
							begin
							
								if(rising_edge_counter == 1)
								begin
									read_state 				<= 1;
									rising_edge_counter		<= 0;
									divReset 				<= 1;
								end
							
								else 
								begin
									rising_edge_counter <= rising_edge_counter +1;
								end
							
							end
						
							else
							begin
							
							end
						
						end
					
						//strobe
						1 : begin
							divReset 	<= 0;
							CS			<= 0;
							//detect rising edge on 25MHz clk
							//on each rising edge increment counter
							if(Z==0 && Y == 1)
							begin
							
								if(rising_edge_counter == 15)
								begin
									read_state 				<= 2;
									rising_edge_counter		<= 0;
									divReset 				<= 1;
									SCLK					<= 0;
								end

								else
								begin
									rising_edge_counter <= rising_edge_counter +1;
									SCLK				<= 1;
									sr0 <= {sr0[15:1],DI0};
									sr1 <= {sr1[15:1],DI1};
									sr2 <= {sr2[15:1],DI2};
									sr3 <= {sr3[15:1],DI3};
								end
							end
						
							else
							begin
								SCLK	<= 0;
							end
						
						end
					
						//disconnect
						//copy shift registers to output registers
						2 : begin
							ADC_DATA0				<= sr0;
							ADC_DATA1				<= sr1;
							ADC_DATA2				<= sr2;
							ADC_DATA3				<= sr3;
							rising_edge_counter		<= 0;
							divReset				<= 1;
							next_state 				<= Wait;
						end
					
					endcase	
				end
			
				//the wait state,
			
				//Raise CS
				//wait out >1000ns before going
				//back to the initial state
				//to wait for another falling
				//edge on the ADC_CLK
				Wait : begin
					CS			<= 1;
					divReset	<= 0;
					//X			<= clk25MHz;
					//Y			<= X;
					//Z			<= Y;
				
					if(Z==0 && Y == 1)
					begin
						//if 1000 ns has passed then next state
						if(rising_edge_counter == 24)
						begin
							next_state	<= Reset;
							divReset	<= 1;
						end
				
						//else increment a counter or something
						else
						begin
							rising_edge_counter <= rising_edge_counter +1;
						end
					end
				end
			
			endcase
		end
	
	
endmodule 
*/