`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:05:41 05/25/2018
// Design Name:   FIFO32x8
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/FIFO_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FIFO32x8
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module FIFO_tb;

	// Inputs
	reg [31:0] in0;
	reg [31:0] in1;
	reg [31:0] in2;
	reg [31:0] in3;
	reg [1:0] inNum;
	reg latchIn;
	reg latchOut;
	reg reset;
	reg clk;

	// Outputs
	wire [7:0] out;
	wire isFull;
	wire isEmpty;
	
	integer i = 0;

	// Instantiate the Unit Under Test (UUT)
	FIFO32x8 uut (
		.in0(in0), 
		.in1(in1), 
		.in2(in2), 
		.in3(in3), 
		.inNum(inNum), 
		.latchIn(latchIn), 
		.latchOut(latchOut), 
		.reset(reset), 
		.clk(clk), 
		.out(out), 
		.isFull(isFull), 
		.isEmpty(isEmpty)
	);



	initial begin
		// Initialize Inputs
		in0 = 0;
		in1 = 0;
		in2 = 0;
		in3 = 0;
		inNum = 0;
		latchIn = 0;
		latchOut = 0;
		reset = 0;
		clk = 0;
		
		//reset
		reset = 1;
		#10;
		reset = 0;	
		#10;

		// Wait 100 ns for global reset to finish
		#25;
        
		// Add stimulus here 
		in0 = "high";
		in1 = "eyan";
		in2 = "bugs";
		in3 = "ript";
		#10;
		
		//load one number
		#10;
		inNum = 2'b11;
		#10;
		
		latchIn = 0;
		#10;
		latchIn = 1;
		#10;
		
		latchIn = 0;
		latchOut = 0;
		#10;
		latchIn = 1;
		latchOut = 1;
		#10;
		latchIn = 0;
		latchOut = 0;
		#10;
		
		for( i = 0; i < 64; i=i+1)
		begin
			
			latchOut = 1;
			#10;
			latchOut = 0;
			#10;
			
			
		end
		
		#20;
		//reset
		reset = 1;
		
		#10;
		reset = 0;
	end
	
	always 
       #5  clk =  ! clk;
      
endmodule

