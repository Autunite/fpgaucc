`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer: George Bushnell
// 
// Create Date:    13:33:17 05/23/2018 
// Design Name: 
// Module Name:    fourfourmultiplexor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This device is a specialized multiplexor. There are four input ports and and a selection
// port for each input. The selection ports select which of the inputs will be outputted to the four
// output ports. Any number but 0 of the selection ports can be selected. If zero of the selection ports
// are selected then the output ports will be set to high impedance. If less than 4 of the selection ports
// are selected then those selections will be outputed starting with out0, along with a 1:0 port representing
// the number of ports selected. So if say 1, 3 are selected then those values will appear on out ports, 0, 1
// along with the value 01 being outputted on the num port.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fourfourmultiplexor(
    input [31:0] in0,
    input [31:0] in1,
    input [31:0] in2,
    input [31:0] in3,
    input [3:0] sel,
    output reg [31:0] out0,
    output reg [31:0] out1,
    output reg [31:0] out2,
    output reg [31:0] out3,
    output reg [1:0] num
    );

//large grouping of case statments for all sixteen possible selection combination,
//default outputs high z on all ports

always@(*)
begin
	case(sel)
		4'b0001 : 
		begin
			out0 <= in0;
			out1 <= 32'bz;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b00;
		end
		
		4'b0010 : 
		begin
			out0 <= in1;
			out1 <= 32'bz;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b00;
		end
		
		4'b0011 : 
		begin
			out0 <= in0;
			out1 <= in1;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b01;
		end
		
		4'b0100 : 
		begin
			out0 <= in2;
			out1 <= 32'bz;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b00;
		end
		
		4'b0101 : 
		begin
			out0 <= in0;
			out1 <= in2;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b01;
		end
		
		4'b0110 : 
		begin
			out0 <= in1;
			out1 <= in2;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b01;
		end
		
		4'b0111 : 
		begin
			out0 <= in0;
			out1 <= in1;
			out2 <= in2;
			out3 <= 32'bz;
			num <= 2'b10;
		end
		
		4'b1000 : 
		begin
			out0 <= in3;
			out1 <= 32'bz;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b00;
		end
		
		4'b1001 : 
		begin
			out0 <= in0;
			out1 <= in3;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b01;
		end
		
		4'b1010 : 
		begin
			out0 <= in1;
			out1 <= in3;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b01;
		end
		
		4'b1011 : 
		begin
			out0 <= in0;
			out1 <= in1;
			out2 <= in3;
			out3 <= 32'bz;
			num <= 2'b10;
		end
		
		4'b1100 : 
		begin
			out0 <= in2;
			out1 <= in3;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'b01;
		end
		
		4'b1101 : 
		begin
			out0 <= in0;
			out1 <= in2;
			out2 <= in3;
			out3 <= 32'bz;
			num <= 2'b10;
		end
		
		4'b1110 : 
		begin
			out0 <= in1;
			out1 <= in2;
			out2 <= in3;
			out3 <= 32'bz;
			num <= 2'b10;
		end
		
		4'b1111 : 
		begin
			out0 <= in0;
			out1 <= in1;
			out2 <= in2;
			out3 <= in3;
			num <= 2'b11;
		end
		
		//in case none are selected
		//shouldn't happen
		default: begin
			out0 <= 32'bz;
			out1 <= 32'bz;
			out2 <= 32'bz;
			out3 <= 32'bz;
			num <= 2'bzz;
		end
	
	endcase
end

endmodule
