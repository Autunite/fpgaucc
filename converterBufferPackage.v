`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:46:31 05/29/2018 
// Design Name: 
// Module Name:    converterBufferPackage 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module converterBufferPackage(
	//this block is so up two four 16b inputs can be used
    input [15:0] sixteenBitInput0,
    input [15:0] sixteenBitInput1,
    input [15:0] sixteenBitInput2,
    input [15:0] sixteenBitInput3,
	 
	 //character output, connect to uart
	 output [7:0] charOut,
	 
	 //dataline to indicate the fifo cannot accept four more 16 bit inputs
	 output isFull,
	 
	 //dataline to indicate the fifo is completely empty
	 output isEmpty,
	 
	 //consider using a rising edge detect
	 //high triggered input to latch in another couple of characters into the fifo
	 input latchIn,
	 
	 
	 //high triggered input to latch out a single character to the charOut output
	 input latchOut,
	 
	 //reset line used for clearing the FIFO
	 input reset,
	 
	 //standard clock used for driving all the circuits
	 //set to 9.5 MHz
     input clk,
	 
	 //four bit input for selecting how many of the 16 char inputs to latch in at once
	 input [3:0] sel
    );

// these wires connect to the input and output of the multiplexor
wire [31:0] ASCIIHex0, ASCIIHex1, ASCIIHex2, ASCIIHex3, SelASCII0, SelASCII1, SelASCII2, SelASCII3;

//wire connects to the fifo to indicate how many characters to latch in
wire [1:0] num;



//block of four binary to ascii converters
////////////////////////////////////////////
binaryToASCIIHex BTAH0 (
	 .binaryinput(sixteenBitInput0), 
	 .clk(clk),
     .ASCIIHexOutput(ASCIIHex0)
);

binaryToASCIIHex BTAH1 (
	 .binaryinput(sixteenBitInput1),
	 .clk(clk),
     .ASCIIHexOutput(ASCIIHex1)
);

binaryToASCIIHex BTAH2 (
	 .binaryinput(sixteenBitInput2),
	 .clk(clk),
     .ASCIIHexOutput(ASCIIHex2)
);

binaryToASCIIHex BTAH3 (
	 .binaryinput(sixteenBitInput3),
	 .clk(clk),
     .ASCIIHexOutput(ASCIIHex3)
);
////////////////////////////////////////////

//multiplexor that selects up to four inputs to use for the fifo
fourfourmultiplexor FFM1 (
	.in0(ASCIIHex0), 
    .in1(ASCIIHex1), 
    .in2(ASCIIHex2), 
    .in3(ASCIIHex3), 
    .sel(sel), 
    .out0(SelASCII0), 
    .out1(SelASCII1), 
    .out2(SelASCII2), 
    .out3(SelASCII3), 
    .num(num)
);

SR17x8 S1(
	.in0(SelASCII0), 
    .in1(SelASCII1), 
    .in2(SelASCII2), 
    .in3(SelASCII3), 
    .inNum(num), 
    .latchIn(latchIn), 
    .latchOut(latchOut), 
    .reset(reset), 
    .clk(clk), 
    .out(charOut), 
    .isFull(isFull), 
    .isEmpty(isEmpty)
    );





/*
//64x8 fifo
FIFO32x8 FIFO1 (
	.in0(SelASCII0), 
    .in1(SelASCII1), 
    .in2(SelASCII2), 
    .in3(SelASCII3), 
    .inNum(num), 
    .latchIn(latchIn), 
    .latchOut(latchOut), 
    .reset(reset), 
    .clk(clk), 
    .out(charOut), 
    .isFull(isFull), 
    .isEmpty(isEmpty)
);
*/


endmodule
