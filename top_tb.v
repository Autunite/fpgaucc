`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:45:34 06/21/2018
// Design Name:   UCC_TOP
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/UCC_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: UCC_TOP
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Test bench for testing the entire system
// 
////////////////////////////////////////////////////////////////////////////////

module top_tb;

	reg [7:0] input_char = 0;				//character used to load into the rx line
	reg [7:0] command_char = "�";			//character used to alert the clock commander
	reg [7:0] set_sample_rate =8'h7D;//BE;	   	//set the sample rate to 125 ks/s
	reg [7:0] set_sequence_length = 8'd209;	//set the sequence length to 1000 samples
	reg [7:0] num_adc = 8'hFF;				//set the number of inputs to four adc 
	reg [7:0] start_sampling = 8'd191;		//tells the adc to start sampling
	reg [7:0] stop_sampling = 8'h192;		//tell	the adc to stop sampling
	
	//adc input value regs
	reg [15:0] ad0 = 16'h9653;
	reg [15:0] ad1 = 16'hBCBD;
	reg [15:0] ad2 = 16'hCDCE;
	reg [15:0] ad3 = 16'hFABA;
	reg [3:0] count = 15;
	
	integer i = 0;

	// Inputs     //reg
	reg fourtyMHzClk;
	reg reset;
	
	reg tch0;
	reg tch1;
	reg tch2;
	
	
	reg DI0;
	reg DI1;
	reg DI2;
	reg DI3;
	reg rx_in;

	// Outputs    //wire
	wire tx_out;
	wire serial_clk;
	wire adc_clk;
	wire data_out;
	wire is_full;
	wire is_empty;
	wire heart1;
	wire heart2;
	
	wire cmp0;
	wire cmp1;
	wire cmp2;
	
	wire [15:0] xormaxmin;

	// Instantiate the Unit Under Test (UUT)
	topModule uut ( 
		.fourtyMHzClk(fourtyMHzClk),
		.reset(reset),
		
		//IO for the fanComparatorWrapper
		.tch0(tch0),
		.tch1(tch1),
		.tch2(tch2),
		.cmp0(cmp0),
		.cmp1(cmp1),
		.cmp2(cmp2),
		
		
		
		//IO for the UCC
		.DI0(DI0),
		.DI1(DI1),
		.DI2(DI2),
		.DI3(DI3),
		.serial_clk(serial_clk),
		.adc_clk(adc_clk),
		.data_out(data_out),
		
		.rx_in(rx_in),
		.tx_out(tx_out),
		
		.is_full(is_full),
		.is_empty(is_empty),
		.heart1(heart1),
		.heart2(heart2),
		
		//dummy load. remove this in the final product
		//only here to prevent synth from optimizing out
		//the max min
		.xormaxmin(xormaxmin)
		
		
	);

	initial begin
		// Initialize Inputs
		fourtyMHzClk 	= 0;
		reset 			= 0;
	
		tch0 			= 0;
		tch1 			= 0;
		tch2 			= 0;
	
	
		DI0 			= 0;
		DI1 			= 0;
		DI2 			= 0;
		DI3 			= 0;
		rx_in		 	= 1;

		

		// Wait 100 ns for global reset to finish
		#100;
		// Add stimulus here
		reset = 1;
		#300;
		reset = 0;
		#300;
		
		DI0 = ad0[count];
		DI1 = ad1[count];
		DI2 = ad2[count];
		DI3 = ad3[count];
		count = 14;
		#80;
		
		#400000;
		
		
		
		//send commands to the clock commander
		//to begin sampling at 190k samples/second
		//for 1000 samples	
		
		/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = set_sample_rate; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			///////////////////////////////////////////////////// 
			
			
			
			/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = set_sequence_length; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			
			/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = num_adc; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			
		/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = start_sampling; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						rx_in = 0;
					end
				if(i == 9)
					begin
						rx_in = 1;
					end
				if(i > 0 && i < 9)
					begin
						rx_in = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			
			//wait a bit then stop sampling
			
		#2000000;	
			
			
		

	end
      
	  //loads data into the SPI
	  always@(posedge serial_clk)
		begin
					DI0 <= ad0[count];
					DI1 <= ad1[count];
					DI2 <= ad2[count];
					DI3 <= ad3[count];
					count <= count - 1;
		end
				
		//changes the values of the inputs over tiem
		always@(negedge adc_clk)
			begin
				ad0 = ad0 -1;
				ad1 = ad1 -1;
				ad2 = ad2 +1;
				ad3 = ad3 +1;
			end
				
				
		//generates clock
		always
  begin 
  //40MHz clk
    #12.5 fourtyMHzClk = ~fourtyMHzClk;
  end
	  
endmodule

