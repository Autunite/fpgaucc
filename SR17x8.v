`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:02:19 07/11/2018 
// Design Name: 
// Module Name:    SR17x8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This device is a shift register memory that takes in up to 4 32 bit inputs and 
// breaks them up into 8 bit characters and places them in fixed locations in memory.
// It does this using a mux, a 17x8 shift register, 5 bit pointer, and a 2 bit imput indicating the number of 32bit 
// pieces of data currently incoming. At the end of the inputed data an 8 bit number representing an 
// ASCII comma is appended, and the pointer is properly incremented to take into account the characters
// placed in and the ending comma.
//
// When the shift out line is asserted the pointer is decremented by one and all the data is shifted up
// by one address, with the lowest address (0) being erased. 
//
// Reset clears all memory
//
// If the memory is full to the point that the next four data pieces threaten to overfill it then isFull
// is asserted and the memory refuses to take in more data.
//
// When latch in is set to one, the data is latched in one full clock cycle after the first clk rising
// edge detects the change in latch in
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module SR17x8(
    input [31:0] in0,
    input [31:0] in1,
    input [31:0] in2,
    input [31:0] in3,
    input [1:0] inNum,
    input latchIn,
    input latchOut,
    input reset,
    input clk,
    output [7:0] out,
    output 		 isFull ,
	output  	isEmpty
    );
	 
	
	//wires reg's instantiation
	
	reg [4:0] fillPointer = 0;
	reg [4:0] iterator = 0;
	
	//memory block instantiation
	reg [7:0] ram_array [16:0];
	
	//assignment to update the isEmpty flag
	assign isEmpty = (fillPointer==0) ? 1:0;
	assign isFull = !isEmpty;
	
	assign out = ram_array[0];
	
	//main part of the module
	//set to zero upon reset
	//when writing to the ram make sure that
	//latchIn and latchOut are 1 for only one clock cycle
	always@(posedge clk)
	begin
	//reset memory to 0
		if(reset)
		begin
			fillPointer <= 5'b00000;
			
			//out 		<= 8'h00;
			for(iterator = 0 ; iterator <17; iterator = iterator +1)
			begin
				ram_array[iterator] <= 0;
			end
		end
		
		/*
			case statement to control reading and writing actions to the
			SR. Should handle all cases, as long as inNum is always correct
			i.e. if a write action is done and in num is 00 then it will always
			assume that one chunk of data was sent, even if nothing was written
			to the input.
		*/
		//put in full, empty, write, read, inNum for the case sensitivity list
		else 
		begin 
			casez({isFull, isEmpty, latchIn, latchOut, inNum})
				
				//assigned everything to itself to try
				//to prevent latch generation
				
				default : 
				begin 
						//out 				<= out;
						fillPointer			<= fillPointer;
						ram_array[0] 		<= ram_array[0];
						ram_array[1] 		<= ram_array[1];
						ram_array[2] 		<= ram_array[2];
						ram_array[3] 		<= ram_array[3];
						ram_array[4] 		<= ram_array[4];
						ram_array[5] 		<= ram_array[5];
						ram_array[6] 		<= ram_array[6];
						ram_array[7] 		<= ram_array[7];
						ram_array[8] 		<= ram_array[8];
						ram_array[9] 		<= ram_array[9];
						ram_array[10] 		<= ram_array[10];
						ram_array[11] 		<= ram_array[11];
						ram_array[12] 		<= ram_array[12];
						ram_array[13] 		<= ram_array[13];
						ram_array[14] 		<= ram_array[14];
						ram_array[15] 		<= ram_array[15];
						ram_array[16] 		<= ram_array[16];
				end
				
				//commented out because theoretically write should never
				//be asserted at the same time as read
				/*	
				// buffer isn't full, isn't empty, read asserted, write asserted
				6'b0011?? : 
				begin
					
					
					//inNum 0
					if(inNum == 2'b00)
					begin
						writeAddr 					<= writeAddr +5;
						ram_array[0]	 	<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2]	 	<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= ",";
						//add in read statement
						//update read address
						readAddr 					<= readAddr+1;
						out							<= ram_array[readAddr];
					end
				
					//inNum 1
					else if(inNum == 2'b01)
					begin
						writeAddr 					<= writeAddr +9;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= in1[31:24];
						ram_array[5] 		<= in1[23:16];
						ram_array[6] 		<= in1[15:8];
						ram_array[7] 		<= in1[7:0];
						ram_array[8] 		<= ",";
						//add in read statement
						//update read address
						readAddr 					<= readAddr+1;
						out							<= ram_array[readAddr];
					
					end
				
					//inNum 2
					else if(inNum == 2'b10)
					begin
						writeAddr 						<= writeAddr +13;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= in1[31:24];
						ram_array[5] 		<= in1[23:16];
						ram_array[6] 		<= in1[15:8];
						ram_array[7] 		<= in1[7:0];
						ram_array[8] 		<= in2[31:24];
						ram_array[9] 		<= in2[23:16];
						ram_array[10] 		<= in2[15:8];
						ram_array[11] 		<= in2[7:0];
						ram_array[12] 		<= ",";
						//add in read statement
						//update read address
						readAddr 					<= readAddr+1;
						out							<= ram_array[readAddr];
					
					end
				
					//inNum 3
					else if(inNum == 2'b11)
					begin
						writeAddr 					<= writeAddr +17;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= in1[31:24];
						ram_array[5] 		<= in1[23:16];
						ram_array[6] 		<= in1[15:8];
						ram_array[7] 		<= in1[7:0];
						ram_array[8] 		<= in2[31:24];
						ram_array[9] 		<= in2[23:16];
						ram_array[10] 		<= in2[15:8];
						ram_array[11] 		<= in2[7:0];
						ram_array[12] 		<= in3[31:24];
						ram_array[13] 		<= in3[23:16];
						ram_array[14] 		<= in3[15:8];
						ram_array[15] 		<= in3[7:0];
						ram_array[16] 		<= ",";
						//add in read statement
						//update read address
						readAddr 					<= readAddr+1;
						out							<= ram_array[readAddr];
					
					end
				
				end
				
			
				// buffer is full and an attempt to write is made
				6'b1?1??? : 
				begin
			
				end
			
				// buffer is empty and an attempt to read is made
				6'b?1?1?? : 
				begin
			
				end
				*/
			
				// buffer isn't full, write asserted, read not asserted
				6'b0?10?? : 
				begin
					//inNum 0
					if(inNum == 2'b00)
					begin
						fillPointer 		<= fillPointer +5;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= ",";
					end
				
					//inNum 1
					else if(inNum == 2'b01)
					begin
						fillPointer 		<= fillPointer +9;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= in1[31:24];
						ram_array[5] 		<= in1[23:16];
						ram_array[6] 		<= in1[15:8];
						ram_array[7] 		<= in1[7:0];
						ram_array[8] 		<= ",";
					end
				
					//inNum 2
					else if(inNum == 2'b10)
					begin
						fillPointer 		<= fillPointer +13;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= in1[31:24];
						ram_array[5] 		<= in1[23:16];
						ram_array[6] 		<= in1[15:8];
						ram_array[7] 		<= in1[7:0];
						ram_array[8] 		<= in2[31:24];
						ram_array[9] 		<= in2[23:16];
						ram_array[10] 		<= in2[15:8];
						ram_array[11] 		<= in2[7:0];
						ram_array[12] 		<= ",";
					end
				
					//inNum 3
					else if(inNum == 2'b11)
					begin
						fillPointer 		<= fillPointer +17;
						ram_array[0] 		<= in0[31:24];
						ram_array[1] 		<= in0[23:16];
						ram_array[2] 		<= in0[15:8];
						ram_array[3] 		<= in0[7:0];
						ram_array[4] 		<= in1[31:24];
						ram_array[5] 		<= in1[23:16];
						ram_array[6] 		<= in1[15:8];
						ram_array[7] 		<= in1[7:0];
						ram_array[8] 		<= in2[31:24];
						ram_array[9] 		<= in2[23:16];
						ram_array[10] 		<= in2[15:8];
						ram_array[11] 		<= in2[7:0];
						ram_array[12] 		<= in3[31:24];
						ram_array[13] 		<= in3[23:16];
						ram_array[14] 		<= in3[15:8];
						ram_array[15] 		<= in3[7:0];
						ram_array[16] 		<= ",";
					end
				
				end	
				
				
			
				// buffer isn't empty, write is not asserted, read asserted
				6'b?001?? : 
				begin
					fillPointer			<= fillPointer-1;
					//out 				<= ram_array[0];
					ram_array[0] 		<= ram_array[1];
					ram_array[1] 		<= ram_array[2];
					ram_array[2] 		<= ram_array[3];
					ram_array[3] 		<= ram_array[4];
					ram_array[4] 		<= ram_array[5];
					ram_array[5] 		<= ram_array[6];
					ram_array[6] 		<= ram_array[7];
					ram_array[7] 		<= ram_array[8];
					ram_array[8] 		<= ram_array[9];
					ram_array[9] 		<= ram_array[10];
					ram_array[10] 		<= ram_array[11];
					ram_array[11] 		<= ram_array[12];
					ram_array[12] 		<= ram_array[13];
					ram_array[13] 		<= ram_array[14];
					ram_array[14] 		<= ram_array[15];
					ram_array[15] 		<= ram_array[16];
					ram_array[16] 		<= 0;
				end
					
			endcase
		end 
			
			
	end
	
endmodule