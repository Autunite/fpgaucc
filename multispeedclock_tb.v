`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:57:42 06/19/2018
// Design Name:   multiSpeedClock
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/multispeedclock_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: multiSpeedClock
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module multispeedclock_tb;



	// Inputs
	reg [1:0] seq_len;
	reg sample_state;
	reg [7:0] clock_spd;
	reg clk_in;
	reg reset;
	reg load;

	// Outputs
	wire FIFO_clk;
	wire ADC_clk;

	// Instantiate the Unit Under Test (UUT)
	multiSpeedClock uut (
		.seq_len(seq_len), 
		.sample_state(sample_state), 
		.clock_spd(clock_spd), 
		.clk_in(clk_in), 
		.reset(reset), 
		.load(load), 
		.FIFO_clk(FIFO_clk), 
		.ADC_clk(ADC_clk)
	);

//generate a 400MHz clock
//timeunit 1 ns;
//timeprecision 100 ps;
//bit clk=0; 
//realtime delay=1.25 ns;
 
  always
  begin 
  //400MHz clk
    #1.25 clk_in = ~clk_in;
  end
  
 //end glock generation



	initial begin
		// Initialize Inputs
		seq_len = 0;
		sample_state = 0;
		clock_spd = 0;
		clk_in = 0;
		reset = 0;
		load = 0;

		// Wait 100 ns for global reset to finish
		#100;
		reset =1;
		#5;
		reset = 0;
		#500000;
        
		// Add stimulus here
		//set clock speed to 190khz
		//set sequence length to 10m
		//set sample state to one
		seq_len = 1;
		sample_state = 1;
		clock_spd = 190;
		//clk_in = 0;
		#2.5;
		//
		load = 1;
		#2.5;
		load = 0;
		
		//delay some time and change the seq len and frequency
		#20000000;
		
		seq_len = 2;
		sample_state = 1;
		clock_spd = 7;
		//clk_in = 0;
		#2.5;
		//
		load = 1;
		#2.5;
		load = 0;

	end
      
endmodule

