`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// 
// Engineer: George Bushnell
// 
// Create Date:    13:40:03 08/16/2017 
// Design Name: 
// Module Name:    accumulator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//	This code takes in 8 16 bit integers and sums them all together
//////////////////////////////////////////////////////////////////////////////////
module accumulator
(
    input [15:0] in0,
    input [15:0] in1,
    input [15:0] in2,
    input [15:0] in3,
    input [15:0] in4,
    input [15:0] in5,
    input [15:0] in6,
    input [15:0] in7,
    input en,
    output reg[18:0] sum =19'b1111111111111111111
    );

	
//on posedge of enable add all of the
//the inputs together and place it in
//sum
always @(posedge en)
begin
	sum <= in0+in1+in2+in3+in4+in5+in6+in7;
end

endmodule
