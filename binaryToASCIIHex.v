`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    15:56:14 05/22/2018 
// Design Name: 
// Module Name:    binaryToASCIIHex 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Converts a 16 bit binary value into four ASCII characters, by splitting up the 16bits
// into nibbles and then adding to the nibble based on whether it is from 0-9 or A-F, then recombines 
//	said nibbles.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module binaryToASCIIHex(
    input [15:0] binaryinput,
	input clk,
    output [31:0] ASCIIHexOutput
    );

	wire [7:0] out0;
	wire [7:0] out1;
	wire [7:0] out2;
	wire [7:0] out3;
	
	assign ASCIIHexOutput = {out3,out2,out1,out0};

	nibbleAddr U0 (
    .binaryIn(binaryinput[3:0]),
	.clk(clk),
    .nibbleASCIIOut(out0)
    );
	 
	 nibbleAddr U1 (
    .binaryIn(binaryinput[7:4]),
	.clk(clk),	
    .nibbleASCIIOut(out1)
    );
	 
	 nibbleAddr U2 (
    .binaryIn(binaryinput[11:8]),
	.clk(clk),
    .nibbleASCIIOut(out2)
    );
	 
	 nibbleAddr U3 (
    .binaryIn(binaryinput[15:12]),
	.clk(clk),
    .nibbleASCIIOut(out3)
    );

endmodule
