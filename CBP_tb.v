`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:50:33 05/29/2018
// Design Name:   converterBufferPackage
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/CBP_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: converterBufferPackage
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module CBP_tb;

	// Inputs
	reg [15:0] sixteenBitInput0;
	reg [15:0] sixteenBitInput1;
	reg [15:0] sixteenBitInput2;
	reg [15:0] sixteenBitInput3;
	reg latchIn;
	reg latchOut;
	reg reset;
	reg clk;
	reg [3:0] sel;
	reg [6:0] i = 0;

	// Outputs
	wire [7:0] charOut;
	wire isFull;
	wire isEmpty;

	// Instantiate the Unit Under Test (UUT)
	converterBufferPackage uut (
		.sixteenBitInput0(sixteenBitInput0), 
		.sixteenBitInput1(sixteenBitInput1), 
		.sixteenBitInput2(sixteenBitInput2), 
		.sixteenBitInput3(sixteenBitInput3), 
		.charOut(charOut), 
		.isFull(isFull), 
		.isEmpty(isEmpty), 
		.latchIn(latchIn), 
		.latchOut(latchOut), 
		.reset(reset), 
		.clk(clk), 
		.sel(sel)
	);

	initial begin
		// Initialize Inputs
		sixteenBitInput0 = 0;
		sixteenBitInput1 = 0;
		sixteenBitInput2 = 0;
		sixteenBitInput3 = 0;
		latchIn = 0;
		latchOut = 0;
		reset = 0;
		clk = 0;
		sel = 0;
		
		

		// Wait 100 ns for global reset to finish
		#100;
		
		reset = 1;
		#50;
		reset = 0;
		#50;
        
		// Add stimulus here
		sixteenBitInput0 = 16'h12AB;
		sixteenBitInput1 = 16'h34CD;
		sixteenBitInput2 = 16'h56EF;
		sixteenBitInput3 = 16'hA89F;
		sel = 4'b1111;
		#20;
		
		
		//first test is to fill the fifo and see if the isFull line goes high
		//also test if the fifo will refuse more inputs if it is full
		
		latchIn = 1;
		
		#10;
		latchOut = 1;
		
		
		//second test is to latch out characters piece by piece and see if the isEmpty line
		//goes high when completely empty
		
		/*
		for(i = 0; i < 70; i = i + 1)
		begin
			latchOut = 1;
			#10;
			latchOut = 1;
			#10; 
		end
		*/

	end
      
		always #2631.57895 latchIn =  ! latchIn;
		always #52.6315789 clk = ! clk;
		
endmodule

