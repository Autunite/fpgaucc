`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:02:03 06/20/2018
// Design Name:   clk16_div
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/dividertester.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: clk16_div
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module dividertester;

	// Inputs
	reg clk_in;
	reg reset;

	// Outputs
	wire clk_out;

	// Instantiate the Unit Under Test (UUT)
	clk16_div uut (
		.clk_in(clk_in),
		.reset(reset),
		.clk_out(clk_out)
	);

	initial begin
		// Initialize Inputs
		clk_in = 0;
		reset = 0;
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		reset =1;
		#10;
		reset = 0;

	end
	
	always #5  clk_in =  ! clk_in;
      
endmodule

