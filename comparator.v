`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:51:13 08/22/2017 
// Design Name: 
// Module Name:    comparator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//	This module takes int he averaged period from the divider and compares it against a preset
// set point (d46875). If the period is greater than this set point then the comparator outputs
// a zero, indicating that the fan is spinning too slowly. 
//
// This set point when used with a 6.25MHz clock corresponds to the low end of the fan
// speed according to the fan data sheet. The set point corresponds with a tachometer
// period of 7.5ms or tachometer frequency of 133.33 Hz, or 4000 RPM of the fan
//////////////////////////////////////////////////////////////////////////////////
module comparator(
    input [15:0] period,
    output reg comOut=0
    );
	
always @(period)
begin
	if(period >16'd46875)
		begin
			comOut <= 0;
		end
	else
		begin
			comOut <=1;
		end
end

endmodule
