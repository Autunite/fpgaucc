`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:45:34 06/21/2018
// Design Name:   UCC_TOP
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/UCC_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: UCC_TOP
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module UCC_tb;

	reg [7:0] input_char = 0;				//character used to load into the rx line
	reg [7:0] command_char = "�";			//character used to alert the clock commander
	reg [7:0] set_sample_rate =8'h01;//BE;	   	//set the sample rate to 190 ks/s
	reg [7:0] set_sequence_length = 8'd209;	//set the sequence length to 1000 samples
	reg [7:0] num_adc = 8'hF2;				//set the number of inputs to four adc 
	reg [7:0] start_sampling = 8'd191;		//tells the adc to start sampling
	reg [7:0] stop_sampling = 8'h192;		//tell	the adc to stop sampling
	
	//adc input value regs
	reg [15:0] ad0 = 16'h9653;
	reg [15:0] ad1 = 16'hBCBD;
	reg [15:0] ad2 = 16'hCDCE;
	reg [15:0] ad3 = 16'hFABA;
	reg [3:0] count = 15;
	
	integer i = 0;

	// Inputs
	reg Reset;
	reg DI_0;
	reg DI_1;
	reg DI_2;
	reg DI_3;
	reg RX_IN;

	// Outputs
	wire TX_OUT;
	wire Serial_Clock;
	wire ADC_CLK;
	wire Data_Out;
	wire Is_Full;
	wire Is_Empty;
	wire HeartBeat;

	// Instantiate the Unit Under Test (UUT)
	UCC_TOP uut ( 
		.Reset(Reset), 
		.DI_0(DI_0), 
		.DI_1(DI_1), 
		.DI_2(DI_2), 
		.DI_3(DI_3), 
		.RX_IN(RX_IN), 
		.TX_OUT(TX_OUT), 
		.Serial_Clock(Serial_Clock),
		.ADC_CLK(ADC_CLK),
		.Data_Out(Data_Out),
		.Is_Full(Is_Full), 
		.Is_Empty(Is_Empty),
		.HeartBeat(HeartBeat)
	);

	initial begin
		// Initialize Inputs
		Reset = 1;
		DI_0 = 0;
		DI_1 = 0;
		DI_2 = 0;
		DI_3 = 0;
		RX_IN = 1;

		

		// Wait 100 ns for global reset to finish
		#100;
		// Add stimulus here
		Reset = 0;
		#300;
		Reset = 1;
		DI_0 = ad0[count];
		DI_1 = ad1[count];
		DI_2 = ad2[count];
		DI_3 = ad3[count];
		count = 14;
		#80;
		
		#400000;
		
		
		
		//send commands to the clock commander
		//to begin sampling at 190k samples/second
		//for 1000 samples	
		
		/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = set_sample_rate; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			///////////////////////////////////////////////////// 
			
			
			
			/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = set_sequence_length; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			
			/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = num_adc; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			
		/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = start_sampling; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			
			//wait a bit then stop sampling
			
		#2000000;	
			
		/////////////////////////////////////////////////////////////
		//input an exclamation point to begin the process
		input_char = command_char;
		
		//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26; 
			//transmit the command	  
			//set sample rate
			input_char = stop_sampling; 
			
			//simple loop to transmit a char
		for(i = 0; i < 10; i = i + 1)
			begin
				if(i == 0   )
					begin
						RX_IN = 0;
					end
				if(i == 9)
					begin
						RX_IN = 1;
					end
				if(i > 0 && i < 9)
					begin
						RX_IN = input_char[i-1];
					end
					
				#105.26;
					
			end
			#105.26;
			/////////////////////////////////////////////////////
			

	end
      
	  always@(posedge Serial_Clock)
				begin
					DI_0 <= ad0[count];
					DI_1 <= ad1[count];
					DI_2 <= ad2[count];
					DI_3 <= ad3[count];
					count <= count - 1;
				end
	  
endmodule

